<div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
	<!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->

	<div class="logo">
		<a href="{{ route('admin.index') }}" class="simple-text">
			Quang Huỳnh
		</a>
	</div>

	<div class="sidebar-wrapper">
        <ul class="nav">
            <li class="active">
                <a href="dashboard.html">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a href="user.html">
                    <i class="material-icons">face</i>
                    <p>User Profile</p>
                </a>
            </li>
            <li>
                <a href="table.html">
                    <i class="material-icons">library_books</i>
                    <p>Story</p>
                </a>
            </li>
            <li>
                <a href="typography.html">
                    <i class="material-icons">perm_media</i>
                    <p>Photography</p>
                </a>
            </li>
            <li>
                <a href="typography.html">
                    <i class="material-icons">flight_takeoff</i>
                    <p>Journey</p>
                </a>
            </li>
            <li>
                <a href="typography.html">
                    <i class="material-icons">timeline</i>
                    <p>Timeline</p>
                </a>
            </li>

            <li>
                <a href="typography.html">
                    <i class="material-icons">settings</i>
                    <p>Setting</p>
                </a>
            </li>

            <li class="active-pro">
                <a href="{{ route('auth.logout') }}" onclick="return confirm('Logout ?')">
                    <i class="material-icons">close</i>
                    <p>Log out</p>
                </a>
            </li>


        </ul>
	</div>
</div>