<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/backend/plugins/img/apple-icon.png') }}" />
	<link rel="icon" type="image/png" href="{{ asset('favicon.ico') }}" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Quang Huỳnh | Admin</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('assets/common/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!--  Material Dashboard CSS    -->
    <link href="{{ asset('assets/backend/plugins/material/css/material-dashboard.css') }}" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ asset('assets/backend/plugins/material/css/demo.css') }}" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="{{ asset('assets/common/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>

<body>

	<div class="wrapper">

	    @include('backend.partials.sidebar_master')

	    <div class="main-panel">
			
			@include('backend.partials.nav_master')

			<div class="content">
				@yield('content')
			</div>

			@include('backend.partials.footer_master')
		</div>
	</div>

</body>

	<!--   Core JS Files   -->
	<script src="{{ asset('assets/backend/plugins/material/js/jquery-3.1.0.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/backend/plugins/material/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/backend/plugins/material/js/material.min.js') }}" type="text/javascript"></script>

	<!--  Charts Plugin -->
	<script src="{{ asset('assets/backend/plugins/material/js/chartist.min.js') }}"></script>

	<!--  Notifications Plugin    -->
	<script src="{{ asset('assets/backend/plugins/material/js/bootstrap-notify.js') }}"></script>

	<!--  Google Maps Plugin    -->
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

	<!-- Material Dashboard javascript methods -->
	<script src="{{ asset('assets/backend/plugins/material/js/material-dashboard.js') }}"></script>

	<!-- Material Dashboard DEMO methods, don't include it in your project! -->
	<script src="{{ asset('assets/backend/plugins/material/js/demo.js') }}"></script>

	<script type="text/javascript">
    	$(document).ready(function(){

			// Javascript method's body can be found in assets/js/demos.js
        	demo.initDashboardPageCharts();

    	});
	</script>

</html>
