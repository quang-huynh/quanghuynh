<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

  <title>#quanghuynhh</title>

  <meta property="og:url"                content="quanghuynh.com/" />
  <meta property="og:type"               content="article" />
  <meta property="og:title"              content="Hello World. I am Quang Huynh" />
  <meta property="og:description"        content="Call me #quanghuynhh. I am a Web Developer & Photographer. I love backpacking and writting everything ! If you like my 'Thế Giới'. Please join with me. We'll talk about what makes you happy :)" />
  <meta property="og:image"              content="{{ asset('profile.jpg') }}" />

  <!-- CSS -->
  <link href="{{ asset('assets/frontend/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
  <link href="{{ asset('assets/frontend/css/font-awesome.min.css') }}" rel="stylesheet" media="screen">
  <link href="{{ asset('assets/frontend/css/simple-line-icons.css') }}" rel="stylesheet" media="screen">
  <link href="{{ asset('assets/frontend/css/animate.css') }}" rel="stylesheet">

  <link href="{{ asset('assets/frontend/plugins/aqura/assets/css/master.css') }}" rel="stylesheet">
    
  <!-- Custom styles CSS -->
  <link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet" media="screen">
    

    <script src="{{ asset('assets/frontend/js/modernizr.custom.js') }}"></script>
       
</head>
<body>

    <div id="embed-api-auth-container"></div>
    <div id="chart-container"></div>
    <div id="view-selector-container"></div>

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-86099909-1', 'auto');
    ga('send', 'pageview');

  </script>

    <script>
        (function(w,d,s,g,js,fs){
          g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
          js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
          js.src='https://apis.google.com/js/platform.js';
          fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
        }(window,document,'script'));
    </script>


    <script>

        gapi.analytics.ready(function() {

        /**
         * Authorize the user immediately if the user has already granted access.
         * If no access has been created, render an authorize button inside the
         * element with the ID "embed-api-auth-container".
         */
        gapi.analytics.auth.authorize({
          container: 'embed-api-auth-container',
          clientid: '245068602415-fucqj3i59nse0751rflmidljfeq7hmr8.apps.googleusercontent.com'
        });


        /**
         * Create a new ViewSelector instance to be rendered inside of an
         * element with the id "view-selector-container".
         */
        var viewSelector = new gapi.analytics.ViewSelector({
          container: 'view-selector-container'
        });

        // Render the view selector to the page.
        viewSelector.execute();


        /**
         * Create a new DataChart instance with the given query parameters
         * and Google chart options. It will be rendered inside an element
         * with the id "chart-container".
         */
        var dataChart = new gapi.analytics.googleCharts.DataChart({
          query: {
            metrics: 'ga:sessions',
            dimensions: 'ga:date',
            'start-date': '30daysAgo',
            'end-date': 'yesterday'
          },
          chart: {
            container: 'chart-container',
            type: 'LINE',
            options: {
              width: '100%'
            }
          }
        });


        /**
         * Render the dataChart on the page whenever a new view is selected.
         */
        viewSelector.on('change', function(ids) {
          dataChart.set({query: {ids: ids}}).execute();
        });

      });
</script>

</body>
</html>