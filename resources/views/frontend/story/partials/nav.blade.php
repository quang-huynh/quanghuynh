<div class="navbar navbar-default default classic full" role="navigation">
    <div class="navbar-header">
        <div class="navbar-brand"><a href="{{ route('frontend.story.index') }}"><img src="{{ asset('assets/common/images/logo-black.png') }}" srcset="" alt="" /></a></div>
        <div class="nav-bars-wrapper">
            <div class="nav-bars-inner">
                <div class="nav-bars" data-toggle="collapse" data-target=".navbar-collapse"><span></span></div>
            </div>
            <!-- /.nav-bars-inner --> 
        </div>
        <!-- /.nav-bars-wrapper --> 
    </div>
    <!-- /.nav-header -->
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav sm-vertical">
            <li><a href="{{ route('frontend.homepage') }}">Home</a></li>
            <li class="sub-menu current">
                <a href="#!">Story<span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="blog.html">Blog</a></li>
                    <li><a href="blog2.html">Blog with Sidebar</a></li>
                    <li><a href="blog3.html">Magazine</a></li>
                    <li><a href="blog-post.html">Blog Post</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!--/.nav-collapse -->
    <div class="clearfix"></div>
    <div class="navbar-footer">
        <ul class="social">
            <li> <a href="https://www.facebook.com/toidangdoi/"><i class="ion-social-facebook"></i></a> </li>
            <li> <a href="#"><i class="ion-social-twitter"></i></a> </li>
            <li> <a href="#"><i class="ion-social-instagram"></i></a> </li>
            <li> <a href="#"><i class="ion-social-vimeo"></i></a> </li>
        </ul>
        <p>© 2017 Quang Huỳnh. All rights reserved.</p>
    </div>
    <!-- /.navbar-footer --> 
</div>
<!--/.navbar -->