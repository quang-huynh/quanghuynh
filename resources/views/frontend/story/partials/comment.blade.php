<div class="comment-form-wrapper text-left">
    <h4>Would you like to share your thoughts?</h4>
    <p>Your email address will not be published. Required fields are marked *</p>
    <form class="comment-form" name="form_name" action="#" method="post">
        <div class="name-field">
            <input type="text" name="first" title="Name*"  value="Name*" onfocus="this.value=''" onblur="this.value='Name*'"/>
        </div>
        <div class="email-field">
            <input type="text" name="first" title="Email*"  value="Email*" onfocus="this.value=''" onblur="this.value='Email*'" />
        </div>
        <div class="website-field">
            <input type="text" name="first" title="Website"  value="Website" onfocus="this.value=''" onblur="this.value='Website'" />
        </div>
        <div class="message-field">
            <textarea name="textarea" id="textarea" rows="5" cols="30" title="Enter your comment here..."></textarea>
        </div>
        <input type="submit" value="Submit" name="submit" class="btn btn-submit" />
    </form>
</div>
<!-- /.comment-form-wrapper --> 