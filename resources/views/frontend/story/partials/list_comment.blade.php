<div id="comments" class="text-left">
    <h4>4 Comments on "Tellus Adipiscing Nibh Mattis Ligula"</h4>
    <ol id="singlecomments" class="commentlist">
        <li>
            <div class="message">
                <div class="user"><img alt="" src="" class="avatar" /></div>
                <div class="message-inner">
                    <div class="info">
                        <h5><a href="#">Connor Gibson</a></h5>
                        <div class="meta"> <span class="date">January 14, 2010</span><span class="reply"><a class="link-effect" href="#">Reply</a></span> </div>
                    </div>
                    <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Sed posuere consectetur est at lobortis. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Vestibulum id ligula porta felis euismod semper.</p>
                </div>
            </div>
        </li>
        <li>
            <div class="message">
                <div class="user"><img alt="" src="" class="avatar" /></div>
                <div class="message-inner">
                    <div class="info">
                        <h5><a href="#">Nikolas Brooten</a></h5>
                        <div class="meta"> <span class="date">February 21, 2010</span><span class="reply"><a class="link-effect" href="#">Reply</a></span> </div>
                    </div>
                    <p>Quisque tristique tincidunt metus non aliquam. Quisque ac risus sit amet quam sollicitudin vestibulum vitae malesuada libero. Mauris magna elit, suscipit non ornare et, blandit a tellus. Pellentesque dignissim ornare elit, quis mattis eros sodales ac.</p>
                </div>
            </div>
            <ul class="children">
                <li class="bypostauthor">
                    <div class="message">
                        <div class="user"><img alt="" src="" class="avatar" /></div>
                        <div class="message-inner bypostauthor">
                            <div class="info">
                                <h5><a href="#">Pearce Frye</a></h5>
                                <div class="meta"> <span class="date">February 22, 2010</span><span class="reply"><a class="link-effect" href="#">Reply</a></span> </div>
                            </div>
                            <p>Cras mattis consectetur purus sit amet fermentum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit non.</p>
                        </div>
                    </div>
                    <ul class="children">
                        <li>
                            <div class="message">
                                <div class="user"><img alt="" src="" class="avatar" /></div>
                                <div class="message-inner">
                                    <div class="info">
                                        <h5><a href="#">Nikolas Brooten</a></h5>
                                        <div class="meta"> <span class="date">April 4, 2010</span><span class="reply"><a class="link-effect" href="#">Reply</a></span> </div>
                                    </div>
                                    <p>Nullam id dolor id nibh ultricies vehicula ut id. Cras mattis consectetur purus sit amet fermentum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <div class="message">
                <div class="user"><img alt="" src="" class="avatar" /></div>
                <div class="message-inner">
                    <div class="info">
                        <h5><a href="#">Lou Bloxham</a></h5>
                        <div class="meta"> <span class="date">May 03, 2010</span><span class="reply"><a class="link-effect" href="#">Reply</a></span> </div>
                    </div>
                    <p>Sed posuere consectetur est at lobortis. Vestibulum id ligula porta felis euismod semper. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
            </div>
        </li>
    </ol>
</div>
<!-- /#comments --> 