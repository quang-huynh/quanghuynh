@extends('frontend.layouts.story')


@section('title', 'Story')


@section('content')


<div class="blog-content classic-view single">
    <div class="blog-posts">
        <div class="post">
            <div class="box">
                <div class="post-content">
                    <h1 class="post-title text-center">Tellus Adipiscing Nibh Mattis Ligula</h1>
                    <div class="meta text-center">
                    	<span class="date">12 Nov 2014</span>
                    	<span class="comments"><a href="#">7 Comments</a></span>
                    	<span class="category"><a href="#">Still Life</a></span>
                    </div>
                    <figure>
                    	<img src="{{ asset('uploads/images/temp/blogs/bc1.jpg') }}" alt="" />
                    </figure>

                    <!-- MAIN CONTENT POST -->
                    <div class="main-content-post">
                    	
                    	<p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur. Curabitur blandit tempus porttitor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
	                    <p>Donec sed odio dui consectetur adipiscing elit. Etiam adipiscing tincidunt elit, eu convallis felis suscipit ut. Phasellus rhoncus tincidunt auctor. Nullam eu sagittis mauris. Donec non dolor ac elit aliquam tincidunt at at sapien. Aenean tortor libero, condimentum ac laoreet vitae, varius tempor nisi. Duis non arcu vel lectus. </p>
	                    <div class="divide10"></div>
	                    <blockquote>
	                        <p> Pellentesque non diam et tortor dignissim bibendum. Neque sit amet mauris egestas quis mattis velit fringilla. Curabitur viver justo sed scelerisque. Cras mattis consectetur purus sit amet fermentum. Aenean eu leo quam.</p>
	                        <small class="author">Very important person</small> 
	                    </blockquote>
	                    <div class="divide10"></div>
	                    <h3>Sit Vulputate Bibendum Purus</h3>
	                    <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vestibulum id ligula porta felis euismod semper. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
	                    <div class="collage-wrapper bm20">
	                        <div id="collage-medium" class="collage effect-parent light-gallery">
	                            <div class="collage-image-wrapper">
	                                <div class="overlay">
	                                    <a href="style/images/art/bc1-full.jpg" class="lgitem" data-sub-html="#caption1">
	                                        <img src="{{ asset('uploads/images/temp/blogs/bf1.jpg') }}" style="width:300px; height:300px;" alt="" />
	                                        <div id="caption1" class="hidden">
	                                            <h3>Fusce Consectetur Sollicitudin</h3>
	                                        </div>
	                                    </a>
	                                </div>
	                            </div>
	                            <div class="collage-image-wrapper">
	                                <div class="overlay">
	                                    <a href="style/images/art/bc2-full.jpg" class="lgitem" data-sub-html="#caption2">
	                                        <img src="{{ asset('uploads/images/temp/blogs/bf1.jpg') }}" style="width:450px; height:300px;" alt="" />
	                                        <div id="caption2" class="hidden">
	                                            <h3>Amet Sit Ridiculus</h3>
	                                        </div>
	                                    </a>
	                                </div>
	                            </div>
	                            <div class="collage-image-wrapper">
	                                <div class="overlay">
	                                    <a href="style/images/art/bc3-full.jpg" class="lgitem" data-sub-html="#caption3">
	                                        <img src="{{ asset('uploads/images/temp/blogs/bf1.jpg') }}" style="width:380px; height:300px;" alt="" />
	                                        <div id="caption3" class="hidden">
	                                            <h3>Vulputate Quam Tellus</h3>
	                                        </div>
	                                    </a>
	                                </div>
	                            </div>
	                            <div class="collage-image-wrapper">
	                                <div class="overlay">
	                                    <a href="style/images/art/bc4-full.jpg" class="lgitem" data-sub-html="#caption4">
	                                        <img src="{{ asset('uploads/images/temp/blogs/bf1.jpg') }}" style="width:199px; height:300px;" alt="" />
	                                        <div id="caption4" class="hidden">
	                                            <h3>Pellentesque Ullamcorper Fusce</h3>
	                                        </div>
	                                    </a>
	                                </div>
	                            </div>
	                            <div class="collage-image-wrapper">
	                                <div class="overlay">
	                                    <a href="style/images/art/bc5-full.jpg" class="lgitem" data-sub-html="#caption5">
	                                        <img src="{{ asset('uploads/images/temp/blogs/bf1.jpg') }}" style="width:286px; height:300px;" alt="" />
	                                        <div id="caption5" class="hidden">
	                                            <h3>Parturient Elit Mattis</h3>
	                                        </div>
	                                    </a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <p>Cras mattis consectetur purus sit amet fermentum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nullam id dolor id nibh ultricies vehicula ut id elit. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Donec sed odio dui. Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum. Cras mattis consectetur purus sit amet fermentum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Vestibulum id ligula porta felis euismod semper. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>

                    </div>
                    <!-- END MAIN CONTENT POST -->
	                    
                    <div class="meta tags text-center">
                    	<a href="#">journal</a>, 
                    	<a href="#">illustration</a>, 
                    	<a href="#">design</a>, 
                    	<a href="#">daily</a>
                    </div>


                    <div class="share share-wrapper text-center"> 
                    	<a href="#" class="btn btn-share share-facebook">Share</a> 
                    	<a href="#" class="btn btn-share share-twitter">Tweet</a> 
                    	<a href="#" class="btn btn-share share-pinterest">Pin It</a> 
                    </div>
                    <!-- /.share-wrapper -->


                    <hr />


                    <div class="post-nav-wrapper">
                        <div class="post-nav prev">
                            <div class="meta"><i class="ion-android-arrow-back"></i> Previous Article</div>
                            <h3 class="post-title"><a href="#">Cursus Quam Ullamcorper Cras Ornare Dolor Mollis Nullam</a></h3>
                        </div>
                        <div class="post-nav next">
                            <div class="meta">Next Article <i class="ion-android-arrow-forward"></i></div>
                            <h3 class="post-title"><a href="#">Fringilla Ligula Consectetur Ridiculus Fermentum Cras Dapibus</a></h3>
                        </div>
                    </div>
                    <!-- /.post-nav -->


                    <hr />


                    <div class="about-author text-left">
                        <div class="author-image"> <img alt="" src="" /> </div>
                        <h4>About the Author</h4>
                        <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Maecenas faucibus mollis interdum. Fusce dapibus, tellus ac.</p>
                        <ul class="social">
                            <li> <a href="#"><i class="ion-social-facebook"></i></a> </li>
                            <li> <a href="#"><i class="ion-social-twitter"></i></a> </li>
                            <li> <a href="#"><i class="ion-social-instagram"></i></a> </li>
                            <li> <a href="#"><i class="ion-social-vimeo"></i></a> </li>
                            <li> <a href="#"><i class="ion-social-linkedin"></i></a> </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <!-- .about-author -->

                    <hr />

                    @include('frontend.story.partials.list_comment')
                    
                </div>
                <!-- /.post-content --> 
            </div>
            <!-- /.box --> 
        </div>
        <!-- /.post -->


        @include('frontend.story.partials.comment')
        
    </div>
    <!-- /.blog-posts --> 
</div>
<!-- /.classic-view -->

@endsection



@section('css')
    @parent

@endsection


@section('js')
    @parent

@endsection