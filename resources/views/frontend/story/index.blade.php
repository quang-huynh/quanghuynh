@extends('frontend.layouts.story')


@section('title', 'Story')


@section('content')

<div class="blog-content classic-view">

	<div class="blog-posts">


		<div class="post">
            <div class="box">
              	<div class="post-content">
	                <h2 class="post-title text-center">
	                	<a href="blog-post.html">Tôi tên là Quang Huỳnh. Ligula Tristique Malesuada Venenatis Fermentum</a>
	                </h2>
	                <div class="meta text-center">
	                	<span class="date">12 Nov 2014</span>
	                	<span class="comments"><a href="#">3 Comments</a></span>
	                	<span class="category"><a href="#">Urban</a>, <a href="#">Conceptual</a></span></div>
	                <div class="fotorama-wrapper">
	                  	<div class="fotorama"
						   data-arrows="false"
						   data-nav="thumbs"
							 data-maxheight="100%"
						   data-transition="slide"
						   data-thumbwidth="90"
						   data-thumbheight="60"> 

						   <img src="{{ asset('uploads/images/temp/blogs/bf1.jpg') }}" data-caption="Rusty blue van in the woods" alt="" /> 
						   <img src="{{ asset('uploads/images/temp/blogs/bf2.jpg') }}" data-caption="Green bike on the street" alt="" /> 
						   <img src="{{ asset('uploads/images/temp/blogs/bf3.jpg') }}" data-caption="A look at the busy street" alt="" /> 
						   <img src="{{ asset('uploads/images/temp/blogs/bf4.jpg') }}" data-caption="Seeing through the train" alt="" /> 
						   <img src="{{ asset('uploads/images/temp/blogs/bf5.jpg') }}" data-caption="Empty train streets" alt="" /> 
						   <img src="{{ asset('uploads/images/temp/blogs/bf6.jpg') }}" data-caption="Late night at the subway station" alt="" /> 
						   <img src="{{ asset('uploads/images/temp/blogs/bf7.jpg') }}" data-caption="Riding bicycle with the bestie" alt="" /> 
						</div>
	                  <!-- /.fotorama --> 
	                </div>
	                <!-- /.fotorama-wrapper -->
	                <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient. Curabitur blandit tempus lacinia odio. Nullam quis risus eget urna mollis ornare vel eu leo. Vestibulum id ligula porta felis euismod semper. Nullam id dolor id nibh ultricies vehicula. Nulla vitae elit libero, a pharetra augue. Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Praesent commodo cursus magna, vel scelerisque nisl et. Donec ullamcorper nulla non metus auctor fringilla. Nullam id dolor id nibh ultricies.</p>
	                <p>Etiam porta sem malesuada magna mollis euismod. Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Integer posuere erat a ante venenatis dapibus posuere. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. </p>
	                <div class="text-center"><a href="blog-post.html" class="more">Read More</a></div>
              	</div>
              	<!-- /.post-content --> 
            </div>
            <!-- /.box --> 
                
      	</div>
  		<!-- /.post -->

		<div class="post">
			<div class="box">
				<div class="post-content">
					<h2 class="post-title text-center"><a href="blog-post.html">Tellus Adipiscing Nibh Mattis Ligula</a></h2>
					<div class="meta text-center"><span class="date">12 Nov 2014</span><span class="comments"><a href="#">7 Comments</a></span><span class="category"><a href="#">Still Life</a></span></div>
					<figure class="overlay">
						<a href="blog-post.html">
							<img src="{{ asset('uploads/images/temp/blogs/bc1.jpg') }}" alt="" />
						</a>
					</figure>
					<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient. Curabitur blandit tempus lacinia odio. Nullam quis risus eget urna mollis ornare vel eu leo. Vestibulum id ligula porta felis euismod semper. Nullam id dolor id nibh ultricies vehicula. Nulla vitae elit libero, a pharetra augue. Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Praesent commodo cursus magna, vel scelerisque nisl et. Donec ullamcorper nulla non metus auctor fringilla. Nullam id dolor id nibh ultricies.</p>
					<p>Etiam porta sem malesuada magna mollis euismod. Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Integer posuere erat a ante venenatis dapibus posuere. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. </p>
					<div class="text-center"><a href="blog-post.html" class="more">Read More</a></div>
				</div>
				<!-- /.post-content --> 
			</div>
			<!-- /.box --> 

		</div>
		<!-- /.post -->

		<div class="post">
			<div class="box">
				<div class="post-content">
					<h2 class="post-title text-center"><a href="blog-post.html">Fringilla Quam Bibendum Magna Ullamcorper</a></h2>
					<div class="meta text-center"><span class="date">12 Nov 2014</span><span class="comments"><a href="#">9 Comments</a></span><span class="category"><a href="#">Still Life</a></span></div>
					<figure class="media-wrapper player">
						<iframe width="560" height="232" src="https://www.youtube.com/embed/A1gwjIv99Vc?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen></iframe>
					</figure>
					<p>Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Sed posuere consectetur est at lobortis. Donec sed odio dui. Sed posuere consectetur est at lobortis. Vestibulum id ligula porta felis euismod semper.</p>
					<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Nulla vitae elit libero, a pharetra augue... </p>
					<div class="text-center"><a href="blog-post.html" class="more">Read More</a></div>
				</div>
				<!-- /.post-content --> 
			</div>
			<!-- /.box --> 

		</div>
		<!-- /.post --> 

	</div>
	<!-- /.blog-posts --> 


	</div>
	<!-- /.blog-content -->


	<div class="grid-view">
		<div class="row blog-posts blog-content">
			<div class="post col-sm-12 col-md-6">
				<div class="box">
					<h3 class="post-title"><a href="blog-post.html">Ligula Tristique Malesuada</a></h3>
					<div class="meta"><span class="date">12 Nov 2014</span><span class="category"><a href="#">Urban</a></span></div>
					<figure class="overlay">
						<a href="#"> 
							<img src="{{ asset('uploads/images/temp/blogs/bc1.jpg') }}" alt="" />
						</a>
					</figure>
					<div class="post-content">
						<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nullam quis risus eget urna mollis.</p>
						<a href="blog-post.html" class="more">Read More</a> </div>
						<!-- /.post-content --> 
					</div>
					<!-- /.box --> 
			</div>
			<!-- /.post -->
				
			<hr />

			<div class="post col-sm-12 col-md-6">
				<div class="box">
					<h3 class="post-title"><a href="blog-post.html">Tellus Adipiscing Nibh Mattis</a></h3>
					<div class="meta"><span class="date">12 Nov 2014</span><span class="category"><a href="#">Still Life</a></span></div>
					<figure class="overlay"><a href="#"><img src="{{ asset('uploads/images/temp/blogs/bc1.jpg') }}" alt="" /></a></figure>
					<div class="post-content">
						<p>Aenean lacinia bibendum nulla sed consectetur. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
						<a href="blog-post.html" class="more">Read More</a> </div>
						<!-- /.post-content --> 
				</div>
					<!-- /.box --> 
			</div>
			<!-- /.post -->

			<hr />


			<div class="post col-sm-12 col-md-6">
				<div class="box">
					<h3 class="post-title"><a href="blog-post.html">Ultricies Fusce Porta Elit</a></h3>
					<div class="meta"><span class="date">12 Nov 2014</span><span class="category"><a href="#">Conceptual</a></span></div>
					<figure class="overlay"><a href="#"><img src="{{ asset('uploads/images/temp/blogs/bc1.jpg') }}" alt="" /></a></figure>
					<div class="post-content">
						<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas faucibus mollis interdum. Nulla vitae elit libero.</p>
						<a href="blog-post.html" class="more">Read More</a> </div>
						<!-- /.post-content --> 
				</div>
				<!-- /.box --> 
			</div>
			<!-- /.post -->
			
			<hr />


			<div class="post col-sm-12 col-md-6">
				<div class="box">
					<h3 class="post-title"><a href="blog-post.html">Fringilla Quam Bibendum Magna</a></h3>
					<div class="meta"><span class="date">12 Nov 2014</span><span class="category"><a href="#">Still Life</a></span></div>
					<figure class="overlay"><a href="#"><img src="{{ asset('uploads/images/temp/blogs/bc1.jpg') }}" alt="" /></a></figure>
					<div class="post-content">
						<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec sed odio dui. Curabitur blandit tempus porttitor pharetra.</p>
						<a href="blog-post.html" class="more">Read More</a> </div>
						<!-- /.post-content --> 
				</div>
				<!-- /.box --> 
			</div>
			<!-- /.post -->

			<hr />

		</div>
		<!-- /.blog-posts --> 

	</div>
	<!-- /.grid-view -->

	<div class="pagination text-center">
		<ul>
			<li><a class="btn btn-square btn-icon" href="#"><i class="ion-android-arrow-back"></i></a></li>
			<li class="active"><a class="btn btn-square" href="#"><span>1</span></a></li>
			<li><a class="btn btn-square" href="#"><span>2</span></a></li>
			<li><a class="btn btn-square" href="#"><span>3</span></a></li>
			<li><a class="btn btn-square btn-icon" href="#"><i class="ion-android-arrow-forward"></i></a></li>
		</ul>
	</div>
	<!-- /.pagination --> 


@endsection



@section('css')
    @parent

@endsection


@section('js')
    @parent

@endsection