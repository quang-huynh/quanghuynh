<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <title>Quang Huỳnh | F.O.T.O</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <meta name="robots" content="index, follow"/>
    <link rel="icon" href="/favicon.ico">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <meta property="og:url"                content="quanghuynh.com/" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="Hello World. I am Quang Huynh" />
    <meta property="og:description"        content="Call me #quanghuynhh. I am a Web Developer & Photographer. I love backpacking and writting everything ! If you like my journey. Let join with me. We'll talk about what makes you happy :)" />
    <meta property="og:image"              content="{{ asset('profile.jpg') }}" />

    <!-- CSS -->
    @section('css')  
    <link rel="stylesheet" href="{{ asset('assets/frontend/plugins/inview/css/styles.css') }}" />

    <link rel="stylesheet" href="{{ asset('assets/frontend/css/style.css') }}" />
    <!--[if IE]><script src="assets/js/excanvas.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @show
</head> 
<body>

    <div id="page-wrapper" class="page-wrapper page-photography">


        <div class="preloader">

            <div class="half top-half">
               <img src="{{ asset('assets/common/images/af-quanghuynhh-black.png') }}" alt="preload logo">
            </div>

            <div class="half bottom-half">

                <div class="scroll-info col-xs-6">
                    <h6>Thao tác nhanh</h6>
                    <div class="col-xs-4 br">
                        <div class="keys">
                            <i class="fa fa-caret-up"></i>
                            <i class="fa fa-caret-left"></i>
                            <i class="fa fa-caret-down"></i>
                            <i class="fa fa-caret-right"></i>
                            <span>Use Keyboard Arrows</span>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="mouse-box">
                            <div class="mouse"></div>
                        </div>
                        <span>scroll with your mouse</span>
                    </div>
                    <div class="col-xs-4">
                        <div class="touch">
                            <i class="fa fa-hand-o-right"></i>
                            <i class="pe-7s-phone"></i>
                        </div>
                        <span>Swipe up and down</span>
                    </div>
                </div>
                
            </div>

        </div>


        <!-- Nav -->
        <div class="overlay photo-menu">

            <div class="photo-menu-wrap">

                <div class="side-info">
                    <div class="info-centered">
                        <a href="">Home</a>
                        <a href="#" data-modal="about" class="md-trigger">About</a>
                        <a href="#" data-modal="about" class="md-trigger">Top</a>
                        <a href="#" data-modal="about" class="md-trigger">Discover</a>
                        <a href="#" data-modal="about" class="md-trigger">Albums</a>
                        <div class="menu-social">
                            <a href="https://www.facebook.com/rockingrow" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.instagram.com/quanghuynhh/" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a href="https://www.flickr.com/photos/quanghuynhphotographer/" target="_blank"><i class="fa fa-flickr"></i></a>
                        </div>
                    </div>  
                    <div class="info-footer">
                        <p>Copyright &copy; 2017 Quanghuynhh.<br class="visible-xs"> All rights reserved.</p>
                    </div>              
                </div>

                <div class="photo-list">

                    <a href="#item-1" class="img-link"> 
                        <div class="img-box">
                            <img src="{{ asset('uploads/images/temp/demo.jpg') }}">
                        </div>
                    </a>

                    <a href="#item-2" class="img-link"> 
                        <div class="img-box">
                            <img src="{{ asset('uploads/images/temp/demo.jpg') }}">
                        </div>
                    </a>
                    <a href="#item-3" class="img-link"> 
                        <div class="img-box">
                            <img src="{{ asset('uploads/images/temp/demo.jpg') }}">
                        </div>
                    </a>
                    <a href="#item-4" class="img-link"> 
                        <div class="img-box">
                            <img src="{{ asset('uploads/images/temp/demo.jpg') }}">
                        </div>
                    </a>
                    <a href="#item-5" class="img-link"> 
                        <div class="img-box">
                            <img src="{{ asset('uploads/images/temp/demo.jpg') }}">
                        </div>
                    </a>
                    <a href="#item-6" class="img-link"> 
                        <div class="img-box">
                            <img src="{{ asset('uploads/images/temp/demo.jpg') }}">
                        </div>
                    </a>
                    <a href="#item-7" class="img-link"> 
                        <div class="img-box">
                            <img src="{{ asset('uploads/images/temp/demo.jpg') }}">
                        </div>
                    </a>
                    <a href="#item-8" class="img-link"> 
                        <div class="img-box">
                            <img src="{{ asset('uploads/images/temp/demo.jpg') }}">
                        </div>
                    </a>
                    <a href="#item-9" class="img-link"> 
                        <div class="img-box">
                            <img src="{{ asset('uploads/images/temp/demo.jpg') }}">
                        </div>
                    </a>
                    <a href="#item-10" class="img-link"> 
                        <div class="img-box">
                            <img src="{{ asset('uploads/images/temp/demo.jpg') }}">
                        </div>
                    </a>
                    <a href="#item-11" class="img-link"> 
                        <div class="img-box">
                            <img src="{{ asset('uploads/images/temp/demo.jpg') }}">
                        </div>
                    </a>
                    <a href="#item-12" class="img-link"> 
                        <div class="img-box">
                            <img src="{{ asset('uploads/images/temp/demo.jpg') }}">
                        </div>
                    </a>

                </div>

            </div>

        </div>
        <!--/Nav -->

        <!-- Options Menu -->
        <!-- <div class="options">
            <a href="#" class="toggle-options"></a>

            <div class="inner">

                <h5>Pick a Style</h5>

                <div class="opt">
                    <h6>Theme</h6>
                    <span>Dark</span>
                    <label class="switch">
                        <input type="checkbox">
                        <div class="slider slider-theme round"></div>
                    </label>
                    <span>Light</span>
                    <p>Only Works for Flat Version</p>
                </div>

                <div class="opt">
                    <h6>Version 1</h6>
                    <a href="index.html">    
                        <img src="{{ asset('uploads/images/temp/demo.jpg') }}" alt="home">
                    </a>
                </div>

                <div class="opt">
                    <h6>Version 2</h6>
                    <a href="">    
                        <img src="{{ asset('uploads/images/temp/demo.jpg') }}" alt="home">
                    </a>
                </div>               

            </div>

        </div> -->
        <!-- // options menu-->


        <!-- Description Modal -->
        <div class="md-modal md-effect photo-desc" id="photo-1">

            <!-- close button -->
            <a class="md-close"></a>

            <!-- md-content -->
            <div class="md-content col-md-5 col-sm-8 col-xs-12">

                <h3>Rough Waves</h3>
                <p>But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                <div class="share-photo">
                    <a href="https://www.facebook.com/rockingrow" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/quanghuynhh/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.flickr.com/photos/quanghuynhphotographer/" target="_blank"><i class="fa fa-flickr"></i></a>
                </div>
                 
            </div><!--// md-content -->

        </div>
        <!-- // Description Modal -->

        <!-- Description Modal -->
        <div class="md-modal md-effect photo-desc" id="photo-2">

            <!-- close button -->
            <a class="md-close"></a>

            <!-- md-content -->
            <div class="md-content col-md-5 col-sm-8 col-xs-12">

                <h3>Clear Waters</h3>
                <p>But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                <div class="share-photo">
                    <a href="https://www.facebook.com/rockingrow" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/quanghuynhh/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.flickr.com/photos/quanghuynhphotographer/" target="_blank"><i class="fa fa-flickr"></i></a>
                </div>
                 
            </div><!--// md-content -->

        </div>
        <!-- // Description Modal -->

        <!-- Description Modal -->
        <div class="md-modal md-effect photo-desc" id="photo-3">

            <!-- close button -->
            <a class="md-close"></a>

            <!-- md-content -->
            <div class="md-content col-md-5 col-sm-8 col-xs-12">

                <h3>Beach Walk</h3>
                <p>But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                <div class="share-photo">
                    <a href="https://www.facebook.com/rockingrow" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/quanghuynhh/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.flickr.com/photos/quanghuynhphotographer/" target="_blank"><i class="fa fa-flickr"></i></a>
                </div>
                 
            </div><!--// md-content -->

        </div>
        <!-- // Description Modal -->

        <!-- Description Modal -->
        <div class="md-modal md-effect photo-desc" id="photo-4">

            <!-- close button -->
            <a class="md-close"></a>

            <!-- md-content -->
            <div class="md-content col-md-5 col-sm-8 col-xs-12">

                <h3>Skipping Puddles</h3>
                <p>But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                <div class="share-photo">
                    <a href="https://www.facebook.com/rockingrow" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/quanghuynhh/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.flickr.com/photos/quanghuynhphotographer/" target="_blank"><i class="fa fa-flickr"></i></a>
                </div>
                 
            </div><!--// md-content -->

        </div>
        <!-- // Description Modal -->

        <!-- Description Modal -->
        <div class="md-modal md-effect photo-desc" id="photo-5">

            <!-- close button -->
            <a class="md-close"></a>

            <!-- md-content -->
            <div class="md-content col-md-5 col-sm-8 col-xs-12">

                <h3>Berry Good Berries</h3>
                <p>But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                <div class="share-photo">
                    <a href="https://www.facebook.com/rockingrow" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/quanghuynhh/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.flickr.com/photos/quanghuynhphotographer/" target="_blank"><i class="fa fa-flickr"></i></a>
                </div>
                 
            </div><!--// md-content -->

        </div>
        <!-- // Description Modal -->

        <!-- Description Modal -->
        <div class="md-modal md-effect photo-desc" id="photo-6">

            <!-- close button -->
            <a class="md-close"></a>

            <!-- md-content -->
            <div class="md-content col-md-5 col-sm-8 col-xs-12">

                <h3>Fierce Stare</h3>
                <p>But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                <div class="share-photo">
                    <a href="https://www.facebook.com/rockingrow" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/quanghuynhh/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.flickr.com/photos/quanghuynhphotographer/" target="_blank"><i class="fa fa-flickr"></i></a>
                </div>
                 
            </div><!--// md-content -->

        </div>
        <!-- // Description Modal -->

        <!-- Description Modal -->
        <div class="md-modal md-effect photo-desc" id="photo-7">

            <!-- close button -->
            <a class="md-close"></a>

            <!-- md-content -->
            <div class="md-content col-md-5 col-sm-8 col-xs-12">

                <h3>Nightime Swim</h3>
                <p>But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                <div class="share-photo">
                    <a href="https://www.facebook.com/rockingrow" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/quanghuynhh/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.flickr.com/photos/quanghuynhphotographer/" target="_blank"><i class="fa fa-flickr"></i></a>
                </div>
                 
            </div><!--// md-content -->

        </div>
        <!-- // Description Modal -->

        <!-- Description Modal -->
        <div class="md-modal md-effect photo-desc" id="photo-8">

            <!-- close button -->
            <a class="md-close"></a>

            <!-- md-content -->
            <div class="md-content col-md-5 col-sm-8 col-xs-12">

                <h3>A Day in the Town</h3>
                <p>But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                <div class="share-photo">
                    <a href="https://www.facebook.com/rockingrow" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/quanghuynhh/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.flickr.com/photos/quanghuynhphotographer/" target="_blank"><i class="fa fa-flickr"></i></a>
                </div>
                 
            </div><!--// md-content -->

        </div>
        <!-- // Description Modal -->

        <!-- Description Modal -->
        <div class="md-modal md-effect photo-desc" id="photo-9">

            <!-- close button -->
            <a class="md-close"></a>

            <!-- md-content -->
            <div class="md-content col-md-5 col-sm-8 col-xs-12">

                <h3>Furry Friends</h3>
                <p>But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                <div class="share-photo">
                    <a href="https://www.facebook.com/rockingrow" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/quanghuynhh/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.flickr.com/photos/quanghuynhphotographer/" target="_blank"><i class="fa fa-flickr"></i></a>
                </div>
                 
            </div><!--// md-content -->

        </div>
        <!-- // Description Modal -->

        <!-- Description Modal -->
        <div class="md-modal md-effect photo-desc" id="photo-10">

            <!-- close button -->
            <a class="md-close"></a>

            <!-- md-content -->
            <div class="md-content col-md-5 col-sm-8 col-xs-12">

                <h3>Heir to the Jungle</h3>
                <p>But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                <div class="share-photo">
                    <a href="https://www.facebook.com/rockingrow" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/quanghuynhh/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.flickr.com/photos/quanghuynhphotographer/" target="_blank"><i class="fa fa-flickr"></i></a>
                </div>
                 
            </div><!--// md-content -->

        </div>
        <!-- // Description Modal -->

        <!-- Description Modal -->
        <div class="md-modal md-effect photo-desc" id="photo-11">

            <!-- close button -->
            <a class="md-close"></a>

            <!-- md-content -->
            <div class="md-content col-md-5 col-sm-8 col-xs-12">

                <h3>Plants, Plants, Plants</h3>
                <p>But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                <div class="share-photo">
                    <a href="https://www.facebook.com/rockingrow" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/quanghuynhh/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.flickr.com/photos/quanghuynhphotographer/" target="_blank"><i class="fa fa-flickr"></i></a>
                </div>
                 
            </div><!--// md-content -->

        </div>
        <!-- // Description Modal -->

        <!-- Description Modal -->
        <div class="md-modal md-effect photo-desc" id="photo-12">

            <!-- close button -->
            <a class="md-close"></a>

            <!-- md-content -->
            <div class="md-content col-md-5 col-sm-8 col-xs-12">

                <h3>Walk in the Park</h3>
                <p>But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                <div class="share-photo">
                    <a href="https://www.facebook.com/rockingrow" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/quanghuynhh/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.flickr.com/photos/quanghuynhphotographer/" target="_blank"><i class="fa fa-flickr"></i></a>
                </div>
                 
            </div><!--// md-content -->

        </div>
        <!-- // Description Modal -->


        <!-- Login Modal --> 
        <div class="md-modal md-effect login" id="login">

            <!-- close button -->
            <a class="md-close"></a>

            <!-- form area -->
            <div class="md-content col-md-3 col-sm-6 col-xs-12">

                <form class="login-form" method="post" action="">
                    <div class="field">
                        <input type="text" id="name" name="name" placeholder="Username" value="Guest">
                    </div>
                    <div class="field">
                        <input type="email" id="email" name="email" placeholder="email" value="yourname@example.com">
                    </div>
                    <div class="field">
                        <input type="password" id="password" name="password" placeholder="Password" value="123456">
                    </div>
                    <div class="field">
                        <input id="submit" name="submit" type="submit" value="Join!" class="submit-btn">
                    </div>
                    <div class="field">
                    </div>
               </form>
                 
            </div><!--// form area -->
        </div>
        <!-- // Login Modal -->


        <!-- About Modal --> 
        <div class="md-modal md-effect about-modal" id="about">

            <!-- close button -->
            <a class="md-close"></a>

            <!-- form area -->
            <div class="md-content col-md-4 col-sm-7 col-xs-12">

                <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain.</p> 
                 
            </div><!--// form area -->
        </div>
        <!-- // About Modal -->

        <!-- the overlay for modals -->
        <div class="md-overlay"></div>


        <div class="upper-nav">

            <!-- Logo -->
            <a href="{{ route('frontend.photography.index') }}" class="logo">
                <img src="{{ asset('assets/common/images/af-quanghuynhh.png') }}">
            </a>
            <!-- // Logo End -->

            <!-- Login Button -->
            <a href="#" data-modal="login" class="md-trigger login-btn">LOGIN</a>

            <!-- Open Menu Button -->
            <a href="#" class="toggle-menu">
                <div>
                    <em></em>
                    <em></em>
                    <em></em>
                </div>
            </a>

        </div>
        <!-- // Upper Nav End -->


        <!-- Next/Prev Section Links -->
        <div class="next-prev">
            <a href="#" id="moveUp"><i class="pe-7s-angle-up"></i></a>
            <a href="#" id="moveDown"><i class="pe-7s-angle-down"></i></a>
        </div>


        <!-- Main Content -->
        <section id="fullpage" class="main-content fullpage">

            
            <!-- Header -->
            <header data-anchor="home" class="bg-black section home-section bg-img-13 photo-section">

                <div class="container">

                    <div class="row">
                        <h1>AF Photography</h1>
                    </div>         
                    
                </div>

                <div class="social-links">
                    <a href="https://www.facebook.com/rockingrow" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/quanghuynhh/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.flickr.com/photos/quanghuynhphotographer/" target="_blank"><i class="fa fa-flickr"></i></a>
                </div>

                <a href="#" class="mouse-box">
                    <div class="mouse"></div>
                </a>
                                            
            </header>
            <!-- End Header -->


            <!-- Photo -->
            <section data-anchor="item-1" class="bg-black section bg-img-1 photo-section">

                <div class="container">

                    <div class="row">

                        <div class="section-title">

                            <!-- Likes -->
                            <a href="#" class="likes"><i class="fa fa-heart"></i><span>64</span></a>

                            <a href="#" class="md-trigger" data-modal="photo-1">
                                <i class="pe-7s-plus"></i>
                                <span>Read More</span>
                            </a>

                            <p class="fadeup">Maine, United States</p>
                            <h2 class="fadeup">Rough Waters</h2>
                        </div><!-- // section title -->

                    </div><!--//row-->

                </div><!--//container-->

            </section>
            <!--// Photo -->

            <!-- Photo -->
            <section data-anchor="item-2" class="bg-black section bg-img-2 photo-section">

                <div class="container">

                    <div class="row">

                        <div class="section-title">
                            <!-- Likes -->
                            <a href="#" class="likes"><i class="fa fa-heart"></i><span>43</span></a>

                            <a href="#"  class="md-trigger fadeup" data-modal="photo-2"><i class="pe-7s-plus"></i><span>Read More</span></a>
                            <p class="fadeup">Hawaii, United States</p>
                            <h2 class="fadeup">Clear Waters</h2>
                        </div><!-- // section title -->

                    </div><!--//row-->

                </div><!--//container-->

            </section>
            <!--// Photo -->

            <!-- Photo -->
            <section data-anchor="item-3" class="bg-black section bg-img-3 photo-section">

                <div class="container">

                    <div class="row">

                        <div class="section-title">

                            <!-- Likes -->
                            <a href="#" class="likes"><i class="fa fa-heart"></i><span>75</span></a>

                            <a href="#"  class="md-trigger fadeup" data-modal="photo-3"><i class="pe-7s-plus"></i><span>Read More</span></a>
                            <p class="fadeup">Jersey Shore, New Jersey</p>
                            <h2 class="fadeup">Beach Walk</h2>
                        </div><!-- // section title -->

                    </div><!--//row-->

                </div><!--//container-->

            </section>
            <!--// Photo -->

            <!-- Photo -->
            <section data-anchor="item-4" class="bg-black section bg-img-4 photo-section">

                <div class="container">

                    <div class="row">

                        <div class="section-title">
                            <!-- Likes -->
                            <a href="#" class="likes"><i class="fa fa-heart"></i><span>98</span></a>

                            <a href="#"  class="md-trigger fadeup" data-modal="photo-4"><i class="pe-7s-plus"></i><span>Read More</span></a>
                            <p class="fadeup">Brooklyn, New York</p>
                            <h2 class="fadeup">Skipping Puddles</h2>
                        </div><!-- // section title -->

                    </div><!--//row-->

                </div><!--//container-->

            </section>
            <!--// Photo -->

            <!-- Photo -->
            <section data-anchor="item-5" class="bg-black section bg-img-5 photo-section">

                <div class="container">

                    <div class="row">

                        <div class="section-title">
                            <!-- Likes -->
                            <a href="#" class="likes"><i class="fa fa-heart"></i><span>23</span></a>
                            <a href="#"  class="md-trigger fadeup" data-modal="photo-5"><i class="pe-7s-plus"></i><span>Read More</span></a>
                            <p class="fadeup">Bejing, China</p>
                            <h2 class="fadeup">Berry Good Berries</h2>
                        </div><!-- // section title -->

                    </div><!--//row-->

                </div><!--//container-->

            </section>
            <!--// Photo -->

            <!-- Photo -->
            <section data-anchor="item-6" class="bg-black section bg-img-6 photo-section">

                <div class="container">

                    <div class="row">

                        <div class="section-title">
                             <!-- Likes -->
                            <a href="#" class="likes"><i class="fa fa-heart"></i><span>50</span></a>
                            <a href="#"  class="md-trigger fadeup" data-modal="photo-6"><i class="pe-7s-plus"></i><span>Read More</span></a>
                            <p class="fadeup">Africa</p>
                            <h2 class="fadeup">Fierce Stare</h2>
                        </div><!-- // section title -->

                    </div><!--//row-->

                </div><!--//container-->

            </section>
            <!--// Photo -->

            <!-- Photo -->
            <section data-anchor="item-7" class="bg-black section bg-img-7 photo-section">

                <div class="container">

                    <div class="row">

                        <div class="section-title">
                             <!-- Likes -->
                            <a href="#" class="likes"><i class="fa fa-heart"></i><span>23</span></a>
                            <a href="#"  class="md-trigger fadeup" data-modal="photo-7"><i class="pe-7s-plus"></i><span>Read More</span></a>
                            <p class="fadeup">California, United States</p>
                            <h2 class="fadeup">Nightime Swim</h2>
                        </div><!-- // section title -->

                    </div><!--//row-->

                </div><!--//container-->

            </section>
            <!--// Photo -->

            <!-- Photo -->
            <section data-anchor="item-8" class="bg-black section bg-img-8 photo-section">

                <div class="container">

                    <div class="row">

                        <div class="section-title">
                             <!-- Likes -->
                            <a href="#" class="likes"><i class="fa fa-heart"></i><span>34</span></a>
                            <a href="#"  class="md-trigger fadeup" data-modal="photo-8"><i class="pe-7s-plus"></i><span>Read More</span></a>
                            <p class="fadeup">Florida, United States</p>
                            <h2 class="fadeup">A Day in The Town</h2>
                        </div><!-- // section title -->

                    </div><!--//row-->

                </div><!--//container-->

            </section>
            <!--// Photo -->

            <!-- Photo -->
            <section data-anchor="item-9" class="bg-black section bg-img-9 photo-section">

                <div class="container">

                    <div class="row">

                        <div class="section-title">
                             <!-- Likes -->
                            <a href="#" class="likes"><i class="fa fa-heart"></i><span>21</span></a>
                            <a href="#"  class="md-trigger fadeup" data-modal="photo-9"><i class="pe-7s-plus"></i><span>Read More</span></a>
                            <p class="fadeup">Moscow, Russia</p>
                            <h2 class="fadeup">Furry Friends</h2>
                        </div><!-- // section title -->

                    </div><!--//row-->

                </div><!--//container-->

            </section>
            <!--// Photo -->

            <!-- Photo -->
            <section data-anchor="item-10" class="bg-black section bg-img-10 photo-section">

                <div class="container">

                    <div class="row">

                        <div class="section-title">
                             <!-- Likes -->
                            <a href="#" class="likes"><i class="fa fa-heart"></i><span>23</span></a>
                            <a href="#"  class="md-trigger fadeup" data-modal="photo-10"><i class="pe-7s-plus"></i><span>Read More</span></a>
                            <p class="fadeup">Southern India</p>
                            <h2 class="fadeup">Heir to the Jungle</h2>
                        </div><!-- // section title -->

                    </div><!--//row-->

                </div><!--//container-->

            </section>
            <!--// Photo -->

            <!-- Photo -->
            <section data-anchor="item-11" class="bg-black section bg-img-11 photo-section">

                <div class="container">

                    <div class="row">

                        <div class="section-title">
                             <!-- Likes -->
                            <a href="#" class="likes"><i class="fa fa-heart"></i><span>90</span></a>
                            <a href="#"  class="md-trigger fadeup" data-modal="photo-11"><i class="pe-7s-plus"></i><span>Read More</span></a>
                            <p class="fadeup">Atlanta, Georgia</p>
                            <h2 class="fadeup">Plants, Plants, Plants</h2>
                        </div><!-- // section title -->

                    </div><!--//row-->

                </div><!--//container-->

            </section>
            <!--// Photo -->

            <!-- Photo -->
            <section data-anchor="item-12" class="bg-black section bg-img-12 photo-section">

                <div class="container">

                    <div class="row">

                        <div class="section-title">
                             <!-- Likes -->
                            <a href="#" class="likes"><i class="fa fa-heart"></i><span>87</span></a>
                            <a href="#"  class="md-trigger fadeup" data-modal="photo-12"><i class="pe-7s-plus"></i><span>Read More</span></a>
                            <p class="fadeup">Central Park, New York City</p>
                            <h2 class="fadeup">Walk in the Park</h2>
                        </div><!-- // section title -->

                    </div><!--//row-->

                </div><!--//container-->

            </section>
            <!--// Photo -->


        </section>
        <!--// Main Content -->

    </div>
    <!--// page-wrapper -->



    @section('js')
        <!--===================
        Plugins 
        ====================-->
        <script src="{{ asset('assets/frontend/plugins/inview/js/jquery-1.9.1.min.js') }}"></script>
        <script src="{{ asset('assets/frontend/plugins/inview/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/frontend/plugins/inview/js/modernizr.custom.js') }}"></script>
        <script src="{{ asset('assets/frontend/plugins/inview/js/jquery.fullPage.min.js') }}"></script>
        <script src="{{ asset('assets/frontend/plugins/inview/js/prettify.js') }}"></script>
        <script src="{{ asset('assets/frontend/plugins/inview/js/application.js') }}"></script>
        <script src="{{ asset('assets/frontend/plugins/inview/js/classie.js') }}"></script>
        <script src="{{ asset('assets/frontend/plugins/inview/js/modaleffect.js') }}"></script>
        <script src="{{ asset('assets/frontend/plugins/inview/js/all.js') }}"></script>
    @show

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-86099909-1', 'auto');
      ga('send', 'pageview');

    </script>

</body>
</html>