@extends('frontend.layouts.master')


@section('title', 'Journey')


@section('content')

    
    <!-- Hero section   -->
    <div class="hero-wrap fl-wrap full-height">
        <div class="hero-item">
            <!-- hero-wrap-image-slider-holder  end -->
            <div class="slideshow-holder fl-wrap full-height">
                <div class="overlay"></div>
                <div class="num-holder3"></div>
                <!-- hero-wrap-image-slider  -->
                <div class="slideshow-slider   hero-slider  fl-wrap full-height">
                    <!-- 1  -->
                    <div class="item fl-wrap full-height">
                        <div class="bg"  data-bg="{{ asset('uploads/images/temp/demo.jpg') }}"></div>
                    </div>
                    <!-- 1  end-->
                    <!-- 1  -->
                    <div class="item fl-wrap full-height">
                        <div class="bg"  data-bg="{{ asset('uploads/images/temp/demo.jpg') }}"></div>
                    </div>
                    <!-- 1  end-->
                    <!-- 1  -->
                    <div class="item fl-wrap full-height">
                        <div class="bg"  data-bg="{{ asset('uploads/images/temp/demo.jpg') }}"></div>
                    </div>
                    <!-- 1  end-->
                    <!-- 1  -->
                    <div class="item fl-wrap full-height">
                        <div class="bg"  data-bg="{{ asset('uploads/images/temp/demo.jpg') }}"></div>
                    </div>
                    <!-- 1  end-->
                </div>
                <!-- hero-wrap-image-slider  end -->
            </div>
            <!-- hero-wrap-image-slider-holder  end -->
        </div>
        <div class="hero-wrap-item  left-her alt">
            <div class="container">
                <div class="fl-wrap section-entry">
                    <p>Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit</p>
                    <h2>Welcome  </h2>
                    <h3>My name is Antony Cooper . I creat web and graphic design</h3>
                    <a href="#sec2" class="btn hide-icon custom-scroll-link"  ><i class="fa fa-flag-checkered"></i><span>Let's start</span></a>  
                </div>
            </div>
        </div>
    </div>
    <!-- Hero section   end -->


    <!-- fixed column  -->
    <div class="fixed-column">
        <div class="column-image fl-wrap full-height">
            <div class="bg" data-bg="{{ asset('uploads/images/temp/demo.jpg') }}"></div>
            <div class="overlay"></div>
        </div>
        <div class="bg-title alt"><span>Journal</span></div>
        <div class="progress-bar-wrap">
            <div class="progress-bar"></div>
        </div>
    </div>
    <!-- fixed column  end -->
    <!-- column-wrap  -->
    <div class="column-wrap scroll-content">
        <!--=============== content ===============-->  
        <!-- content   -->               
        <div  class="content">
            <!-- section-->
            <section  data-scrollax-parent="true" class="dec-sec">
                <div class="container">
                    <div class="section-title">
                        <h2>Journal</h2>
                        <p>Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                        <div class="clearfix"></div>
                        <span class="bold-separator"></span>
                    </div>
                    <div class="searh-holder fl-wrap">
                        <div class="searh-inner">
                            <form action="#">
                                <input name="se" id="se" type="text" class="search" placeholder="Search.." value="Search..." />
                                <button class="search-submit" id="submit_btn"><i class="fa fa-search transition"></i> </button>
                            </form>
                        </div>
                    </div>




                </div>
            </section>
            <!-- section end -->


            <!--  to top  -->  
            <div class="small-sec fl-wrap">
                <div class="to-top-wrap"><a class="to-top" href="#"> <i class="fa fa-angle-double-up"></i> Back to Top</a></div>
            </div>
            <!-- to top end--> 
        </div>
        <!-- content end -->
    </div>
    <!-- column-wrap end -->
@endsection



@section('css')
    @parent

@endsection


@section('js')
	@parent

@endsection