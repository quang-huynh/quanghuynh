<header class="main-header">
    <a href="{{ route('frontend.homepage') }}" class="logo-holder"><img src="{{ asset('assets/common/images/logo.png') }}" alt=""></a>
    <!-- info-button -->
    <div class="nav-button" id="open-button">
        <span class="menu-global menu-top"></span>
        <span class="menu-global menu-middle"></span>
        <span class="menu-global menu-bottom"></span>
    </div>
    <!-- info-button end-->
    <div class="show-share isShare"><img src="{{ asset('assets/frontend/plugins/cooper/images/share.png') }}" alt=""></div>
</header>