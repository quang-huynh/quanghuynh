<div class="hero-wrap fl-wrap full-height">
    <div class="hero-item">
        <div class="overlay"></div>
        <div class="bg" data-bg="{{ asset('uploads/images/temp/demo.jpg') }}"></div>
    </div>
    <div class="single-carousel-holder fl-wrap full-height">
        <div class="single-carousel">
            <!-- item    -->
            <div class="item">
                <div class="overlay"></div>
                <div class="bg" data-bg="{{ asset('uploads/images/temp/story-demo-homepage.jpg') }}"></div>
                <div class="carousel-item alt">
                    <h3><a href="#" class="custom-scroll-link">Story</a></h3>
                    <p>"Ta hoặc ai đó, đi đến nơi cần đến, gặp những người cần gặp, vài ba lá trầu và những câu chuyện trần trụi về cuộc sống. Bài học cuộc đời là thứ chính xác ta cần." <br>- Quang Huỳnh</p>
                </div>
            </div>
            <!-- item end   -->
            <!-- item    -->
            <div class="item">
                <div class="overlay"></div>
                <div class="bg" data-bg="{{ asset('uploads/images/temp/photography-demo-homepage.jpg') }}"></div>
                <div class="carousel-item alt">
                    <h3><a href="#">Photography</a></h3>
                    <p>"Đôi khi không cần quá xuất sắc, chụp một "ai đó" nó dễ dàng như việc ăn một viên kẹo - Đầu lưỡi chạm vào vị ngọt tức là chạm vào linh hồn bức hình. Ngọt là đẹp !" <br> - Quang Huỳnh</p>
                </div>
            </div>
            <!-- item end   -->
            <!-- item    -->
            <div class="item">
                <div class="overlay"></div>
                <div class="bg" data-bg="{{ asset('uploads/images/temp/journey-demo-homepage.jpg') }}"></div>
                <div class="carousel-item alt">
                    <h3><a href="#">Journey</a></h3>
                    <p>"Người ta không sống hai cuộc đời để gặp lại tuổi trẻ" <br> - Quang Huỳnh</p>
                </div>
            </div>
            <!-- item end   -->
        </div>
        <div class="customNavigation gals">
            <a class="next-slide transition"><i class="fa fa-angle-right"></i></a>
            <a class="prev-slide transition"><i class="fa fa-angle-left"></i></a>
        </div>
    </div>
</div>