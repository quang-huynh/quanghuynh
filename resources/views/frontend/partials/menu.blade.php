<div class="menu-wrap">
    <div class="menu-inner">
        <!-- menu logo-->
        <a href="{{ route('frontend.homepage') }}" class="menu-logo">
            <img src="{{ asset('assets/common/images/quanghuynhh.png') }}" alt="">
        </a>
        <!-- menu logo end -->
        <div class="hid-men-wrap alt">
            <div id="hid-men">
                <ul class="menu">
                    <li><a href="{{ route('frontend.aboutme') }}">Tui là ai ?</a></li>
                    <li><a href="{{ route('frontend.journey.index') }}">Nhật ký hành trình</a></li>
                    <li><a href="{{ route('frontend.photography.index') }}">Tui có những cuộn film</a></li>
                    <li><a href="{{ route('frontend.story.index') }}">Tuổi Trẻ của hắn</a></li>
                    <li>
                        <a href="#">Tổ chim</a>
                        <ul>
                            <li><a href="{{ route('frontend.timeline.index') }}">Gác xếp bồ câu</a></li>
                            <li><a href="{{ route('frontend.contact') }}">Hòm thư cún con</a></li>
                        </ul>
                    </li>
                </ul>


                <!-- <ul class="menu">
                    <li><a href="#">Tui là ai ?</a></li>
                    <li><a href="#">Nhật ký hành trình</a></li>
                    <li><a href="#">Tui có những cuộn film</a></li>
                    <li><a href="#">Tuổi Trẻ của hắn</a></li>
                    <li>
                        <a href="#">Tổ chim</a>
                        <ul>
                            <li><a href="#">Gác xếp bồ câu</a></li>
                            <li><a href="#">Hòm thư cún con</a></li>
                        </ul>
                    </li>
                </ul> -->
            </div>
        </div>
    </div>
    <div class="morph-shape" id="morph-shape" data-morph-open="M-7.312,0H15c0,0,66,113.339,66,399.5C81,664.006,15,800,15,800H-7.312V0z;M-7.312,0H100c0,0,0,113.839,0,400c0,264.506,0,400,0,400H-7.312V0z">
        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 800" preserveAspectRatio="none">
            <path d="M-7.312,0H0c0,0,0,113.839,0,400c0,264.506,0,400,0,400h-7.312V0z"/>
        </svg>
    </div>
</div>