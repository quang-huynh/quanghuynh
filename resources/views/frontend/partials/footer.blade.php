<footer class="main-footer">
    <a href="{{ route('frontend.contact') }}" class="mail-link"><i class="fa fa-envelope" aria-hidden="true"></i></a>
    <!-- header-social-->
    <div class="footer-social">
        <ul >
            <li><a href="https://www.facebook.com/rockingrow" target="_blank" ><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://www.linkedin.com/in/quang-huynh-026667a6/" target="_blank" ><i class="fa fa-linkedin"></i></a></li>
            <li><a href="https://www.instagram.com/quanghuynhh/" target="_blank" ><i class="fa fa-instagram"></i></a></li>
            <li><a href="https://www.flickr.com/photos/quanghuynhphotographer/" target="_blank" ><i class="fa fa-flickr"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCRXIELZZZi-aAg961YYiN2Q" target="_blank" ><i class="fa fa-youtube"></i></a></li>
        </ul>
    </div>
    <!-- header-social end-->           
    <div class="copyright">&#169; Quang Huỳnh 2017</div>
</footer>