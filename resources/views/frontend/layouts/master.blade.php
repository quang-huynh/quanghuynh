<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="index, follow"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <title>Quang Huỳnh | @yield('title')</title>

    <meta property="og:url"                content="quanghuynh.com/" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="I am Quang Huynh" />
    <meta property="og:description"        content="My hashtag: #quanghuynhh. I am a Web Developer & Photographer. I love backpacking and writting everything ! If you like my journey. Let join with me. We'll talk about what makes you happy :)" />
    <meta property="og:image"              content="{{ asset('profile.jpg') }}" />

    <!-- CSS -->
    @section('css')    
    <link href="{{ asset('assets/frontend/plugins/cooper/css/reset.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('assets/frontend/plugins/cooper/css/plugins.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('assets/frontend/plugins/cooper/css/cooper-style.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('assets/frontend/plugins/cooper/css/color.css') }}" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700,700i" rel="stylesheet">

    @show

    <!-- Custom styles CSS -->
    <link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet" media="screen">

       
</head>
<body>
        <!--================= loader ================-->
        <div class="loader-holder">
            <div class="loader-inner loader-vis">
                <div class="loader"></div>
            </div>
        </div>
        <!--================= loader end ================-->
        <!--================= main start ================-->
        <div id="main">
            <!--================= Header ================-->
            @include('frontend.partials.header')
            <!-- End header -->


            <!--================= menu ================-->
            @include('frontend.partials.menu')
            <!--menu end-->

            @yield('content')

            <!-- footer-->
            @include('frontend.partials.footer')
            <!-- footer end -->


            <!-- Share container  -->
            <div class="share-container isShare"  data-share="['facebook','twitter','googleplus','pinterest','linkedin']"></div>
            <!-- Share container  end-->


        </div>

        <!-- Main end -->

        <!--=============== scripts  ===============-->
        @section('js')
        <script type="text/javascript" src="{{ asset('assets/common/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/common/js/bootstrap.min.js') }}"></script> 
        
        <script type="text/javascript" src="{{ asset('assets/frontend/plugins/cooper/js/plugins.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/frontend/plugins/cooper/js/cooper-scripts.js') }}"></script>
        @show

        <script type="text/javascript" src="{{ asset('assets/frontend/js/app.js') }}"></script>

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-86099909-1', 'auto');
          ga('send', 'pageview');

        </script>

</body>
</html>