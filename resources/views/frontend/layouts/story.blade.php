<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="index, follow"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <title>Quang Huỳnh | @yield('title')</title>

    <meta property="og:url"                content="quanghuynh.com/" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="I am Quang Huynh" />
    <meta property="og:description"        content="My hashtag: #quanghuynhh. I am a Web Developer & Photographer. I love backpacking and writting everything ! If you like my journey. Let join with me. We'll talk about what makes you happy :)" />
    <meta property="og:image"              content="{{ asset('profile.jpg') }}" />

    <!-- CSS -->
    @section('css')   

    <link href="{{ asset('assets/frontend/plugins/juno/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/plugins/juno/css/plugins.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/plugins/juno/revolution/css/settings.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/plugins/juno/revolution/css/layers.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/plugins/juno/revolution/css/navigation.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/plugins/juno/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/plugins/juno/css/color/purple.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/plugins/juno/type/icons.css') }}">
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles CSS -->
    <link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet" media="screen">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @show

       
</head>
<body>
    <div class="content-wrapper">
        @include('frontend.story.partials.nav')



        <div class="side-body">
            <div class="container inner">
                <div class="blog row">
                    <div class="col-sm-8">
                        @yield('content')
                    </div>

                    <aside class="col-sm-4 sidebar">
                        @include('frontend.story.partials.sidebar')
                    </aside>
                    <!-- /column .sidebar --> 

                </div>
                <!-- /.blog --> 
            </div>
            <!-- /.container --> 
        </div>
        <!-- /.side-body --> 

    </div>


        <!--=============== scripts  ===============-->
        @section('js')
        <script async src='https://pepper.swat.io/embed.js?eyJwb3NpdGlvbiI6InJpZ2h0IiwiY29sb3IiOiI4MDhlYzkiLCJjdXN0b21Db2xvciI6dHJ1ZSwiaWNvbiI6ImJ1YmJsZSIsInByb25vdW4iOiJ1cyIsImxhbmd1YWdlIjoiZW4iLCJpbnRyb1RleHQiOiIiLCJjaGFubmVscyI6W1siZmFjZWJvb2siLCJlbGVtaXMiLCJzb2NpYWwiXSxbInR3aXR0ZXIiLCJlbGVtaXNkZXNpZ24iLCJzb2NpYWwiXSxbImluc3RhZ3JhbSIsInVyYmFuc2hvdHMiLCJzb2NpYWwiXSxbInBob25lIiwiKzgwIDU1NSAyMzEgNTcgNDE0IiwiY2xhc3NpYyJdLFsiZW1haWwiLCJtYWlsQGp1bm9zdHVkaW8uY29tIiwiY2xhc3NpYyJdXX0='></script>


        <script type="text/javascript" src="{{ asset('assets/common/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/frontend/plugins/juno/js/bootstrap.min.js') }}"></script> 


        <script type="text/javascript" src="{{ asset('assets/frontend/plugins/juno/revolution/js/jquery.themepunch.tools.min.js?rev=5.0') }}"></script> 
        <script type="text/javascript" src="{{ asset('assets/frontend/plugins/juno/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0') }}"></script> 
        <script type="text/javascript" src="{{ asset('assets/frontend/plugins/juno/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('assets/frontend/plugins/juno/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('assets/frontend/plugins/juno/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('assets/frontend/plugins/juno/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('assets/frontend/plugins/juno/revolution/js/extensions/revolution.extension.video.min.js') }}"></script> 

        <!-- For Stories Layout -->
        <script type="text/javascript" src="{{ asset('assets/frontend/plugins/juno/js/plugins.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('assets/frontend/plugins/juno/js/scripts.js') }}"></script>
        @show

        <script type="text/javascript" src="{{ asset('assets/frontend/js/app.js') }}"></script>

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-86099909-1', 'auto');
          ga('send', 'pageview');

        </script>

</body>
</html>