<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="/favicon.ico">
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

	<title>I am | Quang Huỳnh</title>

	<meta property="og:url"                content="quanghuynh.com/" />
	<meta property="og:type"               content="article" />
	<meta property="og:title"              content="I am Quang Huynh" />
	<meta property="og:description"        content="My hashtag: #quanghuynhh. I am a Web Developer & Photographer. I love backpacking and writting everything ! If you like my 'Thế Giới'. Let join with me. We'll talk about what makes you happy :)" />
	<meta property="og:image"              content="{{ asset('profile.jpg') }}" />

	<!-- CSS -->
	<link href="{{ asset('assets/frontend/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
	<link href="{{ asset('assets/frontend/css/font-awesome.min.css') }}" rel="stylesheet" media="screen">
	<link href="{{ asset('assets/frontend/css/simple-line-icons.css') }}" rel="stylesheet" media="screen">
	<link href="{{ asset('assets/frontend/css/animate.css') }}" rel="stylesheet">

	<link href="{{ asset('assets/frontend/plugins/aqura/assets/css/master.css') }}" rel="stylesheet">
    
	<!-- Custom styles CSS -->
	<link href="{{ asset('assets/frontend/css/hello-page-style.css') }}" rel="stylesheet" media="screen">
    

    <script src="{{ asset('assets/frontend/js/modernizr.custom.js') }}"></script>
       
</head>
<body>

	<!-- Preloader -->

	<div class="page-loader">
		 <div class="vertical-align align-center">
			  <img src="{{ asset('assets/frontend/plugins/aqura/assets/loader/loader.gif') }}" alt="" class="loader-img">
		 </div>
	</div>


	<section id="introduction" class="pfblock-image screen-height">
        <div class="home-overlay"></div>
		<div class="intro q-canvas">
			<section class="no-mb">
				<div class="row">
					<div class="col-sm-12">
						<div class="breadcrumb-fullscreen-parent phone-menu-bg">
							<div class="breadcrumb breadcrumb-fullscreen alignleft small-description overlay almost-black-overlay" style="background-image: url('{{ asset('assets/frontend/plugins/aqura/assets/img/starHomePage/star.jpg') }}');background-position: center;background-size: cover;" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="0">
								<div id="home" style="position: absolute;left: 0;top: 0;">
									<div class="intro-header">
										<div class="js-height-full star" style="height: 955px;">
											<div class="star-pattern-1 js-height-full" style="height: 994px;"></div>
											<div class="col-sm-12"> 
												<div class="starTitle">
													<h4>Hello, i am</h4>
													<div class="grid__item">
								                		<h1>
										                	<a class="link link-yaku" href="#">
																<span>Q</span><span>U</span><span>A</span><span>N</span><span>G</span><span>H</span><span>U</span><span>Y</span><span>N</span><span>H</span>				
															</a>
												        </h1>
								                	</div>
													<h4>Web Developer - Photographer</h4>
												</div>
												<div id="button-start" class="button-start">
													<a href="/" class="btn btn-1 btn-1e">Hug Me :"></a>
												</div>
												<canvas class="cover" width="1920" height="955"></canvas>
											</div>
										</div>
									</div>
			   					</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

        <!-- <a href="#home">
			<div id="leave-introduction" class="scroll-down">
	            <span>
	                <i class="fa fa-angle-down fa-2x"></i>
	            </span>
			</div>
        </a> -->

	</section>

	<!-- Home start -->

	<div id="welcome-page-main-container" class="main-container welcome-page">

		<section id="home" class="pfblock-image screen-height">
	        <div class="home-overlay"></div>
			<div class="intro">
				<div class="start">Hello, my name is Quang Huynh and I am</div>
				<h1>Web Developer</h1>
				<div class="start">create Web Application, Design, Photograph and Writting.</div>
			</div>

	        <a href="#footer">
			<div class="scroll-down">
	            <span>
	                <i class="fa fa-angle-down fa-2x"></i>
	            </span>
			</div>
	        </a>

		</section>

		<!-- Home end -->


		<footer id="footer">
			<div class="container">
				<div class="row">

					<div class="col-sm-12">

						<ul class="social-links">
							<li><a href="https://www.facebook.com/rockingrow" target="_blank" class="wow fadeInUp"><i class="fa fa-facebook"></i></a></li>
							<li><a href="https://www.instagram.com/quanghuynhh/" class="wow fadeInUp" target="_blank" data-wow-delay=".1s"><i class="fa fa-instagram"></i></a></li>
							<li><a href="https://www.youtube.com/channel/UCRXIELZZZi-aAg961YYiN2Q" target="_blank" class="wow fadeInUp" data-wow-delay=".2s"><i class="fa fa-youtube"></i></a></li>
							<li><a href="mailto:quanghh62@gmail.com" class="wow fadeInUp" data-wow-delay=".5s"><i class="fa fa-envelope"></i></a></li>
						</ul>
	                    <p class="copyright">
	                        © 2016 Quang Huynh
						</p>

					</div>

				</div><!-- .row -->
			</div><!-- .container -->
		</footer>

		<!-- Footer end -->

		<!-- Scroll to top -->

		<div class="scroll-up">
			<a href="#home"><i class="fa fa-angle-up"></i></a>
		</div>

	</div>

	
    
    <!-- Scroll to top end-->

	<!-- Javascript files -->

	<script src="{{ asset('assets/frontend/js/jquery-1.11.1.min.js') }}"></script>
	<script src="{{ asset('assets/frontend/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('assets/frontend/plugins/aqura/assets/js/plugins.js') }}"></script>
    <script src="{{ asset('assets/frontend/plugins/aqura/assets/js/main.js') }}"></script>


	<script src="{{ asset('assets/frontend/js/hello-page.js') }}"></script>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-86099909-1', 'auto');
	  ga('send', 'pageview');

	</script>

</body>
</html>