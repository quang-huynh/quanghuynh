@extends('frontend.layouts.master')


@section('title', 'About')


@section('content')
    <!-- fixed column  -->
    <div class="fixed-column">
        <div class="column-image fl-wrap full-height">
            <div class="bg" data-bg="images/bg/long/1.jpg"></div>
            <div class="overlay"></div>
        </div>
        <div class="bg-title alt"><span>About</span></div>
        <div class="progress-bar-wrap">
            <div class="progress-bar"></div>
        </div>
    </div>
    <!-- fixed column  end -->
    <!-- column-wrap  -->
    <div class="column-wrap scroll-content">
        <!--=============== content ===============-->  
        <!-- content   -->               
        <div  class="content  ">
            <!-- section-->
            <section  data-scrollax-parent="true" class="dec-sec">
                <div class="container">
                    <div class="section-title">
                        <h2>About Me</h2>
                        <p>Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                        <div class="clearfix"></div>
                        <span class="bold-separator"></span>
                    </div>
                    <div class="fl-wrap">
                        <div class="single-slider-holder fl-wrap lightgallery">
                            <div class="single-slider fl-wrap" data-loppsli="0">
                                <div class="item">
                                    <img  src="images/folio/1.jpg"  class="respimg"   alt="">
                                    <a data-src="images/folio/1.jpg" class="popup-image slider-zoom">
                                    <i class="fa fa-expand"></i>
                                    </a>
                                </div>
                                <div class="item">
                                    <img  src="images/folio/1.jpg"  class="respimg"   alt="">
                                    <a data-src="images/folio/1.jpg" class="popup-image slider-zoom">
                                    <i class="fa fa-expand"></i>
                                    </a>
                                </div>
                                <div class="item">
                                    <img  src="images/folio/1.jpg"  class="respimg"   alt="">
                                    <a data-src="images/folio/1.jpg" class="popup-image slider-zoom">
                                    <i class="fa fa-expand"></i>
                                    </a>
                                </div>
                                <div class="item">
                                    <img  src="images/folio/1.jpg"  class="respimg"   alt="">
                                    <a data-src="images/folio/1.jpg" class="popup-image slider-zoom">
                                    <i class="fa fa-expand"></i>
                                    </a>
                                </div>
                            </div>
                            <!--  navigation -->
                            <div class="customNavigation gals">
                                <a class="prev-slide transition"> <i class="fa fa-angle-left"></i></a>
                                <a class="next-slide transition"><i class="fa fa-angle-right"></i></a>
                            </div>
                            <!--  navigation end-->
                        </div>
                    </div>
                    <div class="fl-wrap abt-wrap mr-top">
                        <div class="row">
                            <div class="col-md-8">
                                <h3 class="text-title">Quisque varius  <span>eros ac purus</span>  dignissim</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare sem sed quam tempus aliquet vitae eget dolor. Proin eu ultrices libero. Curabitur vulputate vestibulum elementum. Suspendisse id neque a nibh mollis blandit. Quisque varius eros ac purus dignissim.
                                    Proin eu ultrices libero. Curabitur vulputate vestibulum elementum. Suspendisse id neque a nibh mollis blandit. Quisque varius eros ac purus dignissim.
                                </p>
                                <blockquote>
                                    <p> Cras lacinia magna vel molestie faucibus. Donec auctor et urnaLorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacinia magna vel molestie faucibus.Cras lacinia magna vel molestie faucibus. Donec auctor et urnaLorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacinia magna vel molestie faucibus.Cras lacinia magna vel molestie faucibus.   </p>
                                </blockquote>
                                <p>Quisque varius eros ac purus dignissim.
                                    Proin eu ultrices libero. Curabitur vulputate vestibulum elementum. Suspendisse id neque a nibh mollis blandit. Quisque varius eros ac purus dignissim.
                                </p>
                            </div>
                            <div class="col-md-4">
                                <ul class="dec-list">
                                    <li><span>Category </span> : Branding</li>
                                    <li><span>Date </span> : 02.03.2015 </li>
                                    <li><span>Client </span> : Usla Ink</li>
                                    <li><span>Address </span> :   domian.com</li>
                                </ul>
                                <a href="#" class="btn hide-icon mr-top" target="_blank"><i class="fa fa-file-pdf-o"></i><span>View Project</span></a> 
                            </div>
                        </div>
                    </div>
                    <!-- page anv end   -->
                    <div class="page-nav bl-nav">
                        <a class="ajax ppn" href="portfolio-single2.html">
                            <span class="nav-text">Prev</span>
                            <div class="tooltip">
                                <img src="images/folio/1.jpg" class="respimg" alt="" title="">
                                <h5>Duis aliquet</h5>
                            </div>
                        </a>
                        <a class="ajax npn" href="portfolio-single3.html">
                            <span class="nav-text">Next</span>
                            <div class="tooltip">
                                <img src="images/folio/1.jpg" class="respimg" alt="" title="">
                                <h5>Etiam in nulla</h5>
                            </div>
                        </a>
                    </div>
                    <!-- page anv end  -->
                </div>
                <div class="clearfix"></div>
                <div class="container">
                    <div class="order-wrap fl-wrap color-bg">
                        <div class="row">
                            <div class="col-md-8">
                                <h4>Ready to order your project ?</h4>
                            </div>
                            <div class="col-md-4">
                                <a href="contact.html" class="ord-link">Get In Touch</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="parallax-title right-pos" data-scrollax="properties: { translateY: '-350px' }">About Me</div>
            </section>
            <!-- section end -->
            <!--  to top  -->  
            <div class="small-sec fl-wrap">
                <div class="to-top-wrap"><a class="to-top" href="#"> <i class="fa fa-angle-double-up"></i> Back to Top</a></div>
            </div>
            <!-- to top end--> 
        </div>
        <!-- content end -->
    </div>
    <!-- column-wrap end -->
@endsection



@section('css')
    @parent

@endsection


@section('js')
	@parent

@endsection