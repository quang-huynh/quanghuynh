@extends('frontend.layouts.master')


@section('title', 'Home')
@section('content')

            <!-- Hero section   -->
            @include('frontend.partials.ontophomepage')
            <!-- Hero section   end -->



            <!-- fixed column  -->
            <div class="fixed-column">
                <div class="column-image fl-wrap full-height">
                    <div class="bg bg-scroll" ></div>
                    <div class="overlay"></div>
                </div>
                <div class="bg-title alt"><span></span></div>
                <div class="progress-bar-wrap">
                    <div class="progress-bar"></div>
                </div>
            </div>
            <!-- fixed column  end -->


            <!-- column-wrap  -->
            <div class="column-wrap scroll-content">
                <!--=============== content ===============-->	
                <!-- scroll page navigation -->
                <div class="scroll-nav-holder fl-wrap">
                    <nav class="scroll-nav fl-wrap">
                        <ul>
                            <li><a class="scroll-link fbgs" href="#about" data-bgscr="{{ asset('uploads/images/temp/about-head-home.jpg') }}" data-bgtex="about"><span>About me</span></a></li>
                            <li><a class="scroll-link" href="#journey" data-bgscr="{{ asset('uploads/images/temp/journey-head-home.jpg') }}" data-bgtex="Journey"><span>Journey</span></a></li>
                            <li><a class="scroll-link" href="#photography" data-bgscr="{{ asset('uploads/images/temp/photo-head-home.jpg') }}" data-bgtex="F.O.T.O"><span>Photography</span></a></li>
                            <li><a class="scroll-link" href="#stories" data-bgscr="{{ asset('uploads/images/temp/story-head-home.jpg') }}" data-bgtex="Story"><span>Stories</span></a></li>
                            <li><a class="scroll-link" href="#timeline" data-bgscr="{{ asset('uploads/images/temp/timeline-head-home.jpg') }}" data-bgtex="Timeline"><span>Timeline</span></a></li>
                        </ul>
                    </nav>
                </div>
                <!-- scroll page navigation end --> 
                <!-- content   -->               
                <div class="content home-content">

                    <!--=============== section about  ===============-->  
                    <section id="about" data-scrollax-parent="true" class="scroll-con-sec small-pad-sec">
                        <div class="container">
                            <div class="section-title">
                                <h2>About Me</h2>
                                <div class="clearfix"></div>
                                <span class="bold-separator"></span>
                            </div>
                            <div class="fl-wrap abt-wrap">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="box-item vis-det fl-wrap">
                                            <img src="{{ asset('uploads/images/temp/bong-den.jpg') }}" class="respimg" alt="">
                                            <a data-src="{{ asset('uploads/images/temp/demo.jpg') }}" class="image-popup"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h4 class="text-subtitle">Hello,</h4>
                                        <p>Tôi là Quang Huỳnh. Là người con xứ biển, sinh ở Phú Yên, lớn lên tại Bình Dương. Hiện đang sống và làm việc ở Sài Gòn.</p>
                                        <h3 class="text-title"><span>I am Web developer & Photographer</span></h3>
                                        <p>I am a developer with 3 years experience about Web Services. I want to be an full-stack web developer. My skills include back-end and front-end develop. I also have knowledge about UX/UI, design and system analysis. I'm learning the knowledge of artificial intelligence in Ho Chi Minh University of Science.
                                        </p>
                                        <a href="javascript:void(0);" class="btn hide-icon"><i class="fa fa-eye"></i><span>My Cultivated Variety</span></a> 
                                        <div class="fl-wrap">
                                            <div class="skillbar-box animaper">
                                                <!-- skill 1-->
                                                <div class="custom-skillbar-title"><span>PHP</span></div>
                                                <div class="skill-bar-percent">90%</div>
                                                <div class="skillbar-bg" data-percent="90%">
                                                    <div class="custom-skillbar"></div>
                                                </div>
                                                <!-- skill 2-->
                                                <div class="custom-skillbar-title"><span>Front-end</span></div>
                                                <div class="skill-bar-percent">85%</div>
                                                <div class="skillbar-bg" data-percent="85%">
                                                    <div class="custom-skillbar"></div>
                                                </div>
                                                <!-- skill 3-->
                                                <div class="custom-skillbar-title"><span>Design</span></div>
                                                <div class="skill-bar-percent">70%</div>
                                                <div class="skillbar-bg" data-percent="70%">
                                                    <div class="custom-skillbar"></div>
                                                </div>
                                                <!-- skill 4-->
                                                <div class="custom-skillbar-title"><span>UX/UI</span></div>
                                                <div class="skill-bar-percent">80%</div>
                                                <div class="skillbar-bg" data-percent="80%">
                                                    <div class="custom-skillbar"></div>
                                                </div>
                                                <!-- skill 5-->
                                                <div class="custom-skillbar-title"><span>System analysis</span></div>
                                                <div class="skill-bar-percent">70%</div>
                                                <div class="skillbar-bg" data-percent="70%">
                                                    <div class="custom-skillbar"></div>
                                                </div>
                                                <!-- skill 6-->
                                                <div class="custom-skillbar-title"><span>AI</span></div>
                                                <div class="skill-bar-percent">70%</div>
                                                <div class="skillbar-bg" data-percent="70%">
                                                    <div class="custom-skillbar"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <p></p>
                                        
                                        <p></p>
                                        <blockquote>
                                            <p>Là người trầm tính nhưng đa cảm. Nhìn cuộc sống bằng nhiều màu sắc, sự chân thành và một trái tim yêu thương.</p>
                                            <p>Có tình yêu mãnh liệt với nhiếp ảnh, nhất là streetlife photography. Theo đuổi những chuyến đi dài mang theo yêu thương và trái tim chia sẻ. Hoàn thành chuyến đi xuyên Việt một mình ở tuổi 23, hứa hẹn những chuyến đi xa tiếp theo tới những vùng đất mới và thú vị. Thích viết về cuộc sống qua câu chữ đơn giản trần trụi "càng thật-rất thật". Cũng hay đọc sách nhưng toàn tản văn ngắn, chuyện yêu chuyện sống chuyện đi đâu làm gì lúc còn trẻ. Và còn vô vàng thứ khác đang chờ người khác khám phá :"></p>
                                        </blockquote>
                                                                 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="parallax-title right-pos" data-scrollax="properties: { translateY: '-350px' }">AF</div>
                    </section>
                    <!-- section end -->

                    <!--=============== section journey  ===============-->	
                    <section data-scrollax-parent="true"  id="journey" class="scroll-con-sec dec-sec">
                        <div class="container">
                            <div class="section-title">
                                <h2>Journey</h2>
                                <p>Đôi khi trãi nghiệm thật sự không phải cứ là tự mình theo đuổi những con đường mòn và đi đến kết thúc của hành trình. Ta hoặc ai đó, đi đến nơi cần đến, gặp những người cần gặp, vài ba lá chầu và những câu chuyện trần trụi về cuộc sống. Bài học cuộc đời là thứ chính xác ta cần. Chuyến đi, cung đường và chuyện chơi-ăn-ngủ chỉ là cái tất yếu mình phải trãi qua để thỏa mãn cơn nghiện ngập của sở thích. Mà nghiện kiểu này có khi lại hay !</p>
                                <div class="clearfix"></div>
                                <span class="bold-separator"></span>
                            </div>
                            <div class="custom-inner-holder">
                                <div class="row">
                                    <div class="col-md-12">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="parallax-title right-pos" data-scrollax="properties: { translateY: '-350px' }">J'ney</div>
                    </section>
                    <!-- section end -->

                    <!--=============== section photography  ===============-->  
                    <section id="photography" data-scrollax-parent="true" class="scroll-con-sec dec-sec">
                        <div class="sec-dec right-rot"></div>
                        <div class="container">
                            <div class="section-title">
                                <h2>Photography</h2>
                                <p>Đôi khi không cần quá xuất sắc, chụp một "ai đó" nó dễ dàng như việc ăn một viên kẹo - Đầu lưỡi chạm vào vị ngọt tức là chạm vào linh hồn bức hình. Ngọt là đẹp !</p>
                                <div class="clearfix"></div>
                                <span class="bold-separator"></span>
                            </div>
                            <div class="fl-wrap serv-wrap">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="home-photography-tab">
                                            <div class="tabs tabs_animate">
                                                <ul class="horizontal">
                                                    <li><a href="#discover-tab">Discover</a></li>
                                                    <li><a href="#albums-tab">Albums</a></li>
                                                </ul>
                                                <div id="discover-tab" class="q-discover-tab-home">
                                                    
                                                    <div id="gallery-header">
                                                        <div id="gallery-header-center">
                                                            <div id="gallery-header-center-left">
                                                                <!-- <div id="gallery-header-center-left-icon"><span class="iconb" data-icon="&#xe23a;"></span></div> -->
                                                                <div id="gallery-header-center-left-title">All New Photos</div>
                                                            </div>
                                                            <div id="gallery-header-center-right">
                                                                <div class="gallery-header-center-right-links" id="filter-all">All</div>
                                                                <div class="gallery-header-center-right-links" id="filter-film">Film</div>
                                                                <div class="gallery-header-center-right-links" id="filter-journey">Journey</div>
                                                                <div class="gallery-header-center-right-links" id="filter-portrait">Portrait</div>
                                                                <div class="gallery-header-center-right-links" id="filter-landscape">Landscape</div>
                                                                <div class="gallery-header-center-right-links" id="filter-streetlife">Streetlife</div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="gallery-content">
                                                        <div id="gallery-content-center">
                                                            <a data-src="{{ asset('uploads/images/temp/photography/film-1.jpg') }}" class="image-popup"><img src="{{ asset('uploads/images/temp/photography/film-1.jpg') }}" class="all film"/></a>
                                                            <a data-src="{{ asset('uploads/images/temp/photography/film-2.jpg') }}" class="image-popup"><img src="{{ asset('uploads/images/temp/photography/film-2.jpg') }}" class="all film"></a>
                                                            <a data-src="{{ asset('uploads/images/temp/photography/film-3.jpg') }}" class="image-popup"><img src="{{ asset('uploads/images/temp/photography/film-3.jpg') }}" class="all film"/></a>
                                                            <a data-src="{{ asset('uploads/images/temp/photography/journey-1.jpg') }}" class="image-popup"><img src="{{ asset('uploads/images/temp/photography/journey-1.jpg') }}" class="all journey"/></a>
                                                            <a data-src="{{ asset('uploads/images/temp/photography/journey-2.jpg') }}" class="image-popup"><img src="{{ asset('uploads/images/temp/photography/journey-2.jpg') }}" class="all journey"/></a>
                                                            <a data-src="{{ asset('uploads/images/temp/photography/journey-3.jpg') }}" class="image-popup"><img src="{{ asset('uploads/images/temp/photography/journey-3.jpg') }}" class="all journey"/></a>
                                                            <a data-src="{{ asset('uploads/images/temp/photography/portrait-1.jpg') }}" class="image-popup"><img src="{{ asset('uploads/images/temp/photography/portrait-1.jpg') }}" class="all portrait"/></a>
                                                            <a data-src="{{ asset('uploads/images/temp/photography/portrait-2.jpg') }}" class="image-popup"><img src="{{ asset('uploads/images/temp/photography/portrait-2.jpg') }}" class="all portrait"/></a>
                                                            <a data-src="{{ asset('uploads/images/temp/photography/portrait-3.jpg') }}" class="image-popup"><img src="{{ asset('uploads/images/temp/photography/portrait-3.jpg') }}" class="all portrait"/></a>
                                                            <a data-src="{{ asset('uploads/images/temp/photography/portrait-4.jpg') }}" class="image-popup"><img src="{{ asset('uploads/images/temp/photography/portrait-4.jpg') }}" class="all portrait"/></a>
                                                            <a data-src="{{ asset('uploads/images/temp/photography/landscape-1.jpg') }}" class="image-popup"><img src="{{ asset('uploads/images/temp/photography/landscape-1.jpg') }}" class="all landscape"/></a>
                                                            <a data-src="{{ asset('uploads/images/temp/photography/landscape-2.jpg') }}" class="image-popup"><img src="{{ asset('uploads/images/temp/photography/landscape-2.jpg') }}" class="all landscape"/></a>
                                                            <a data-src="{{ asset('uploads/images/temp/photography/landscape-3.jpg') }}" class="image-popup"><img src="{{ asset('uploads/images/temp/photography/landscape-3.jpg') }}" class="all landscape"/></a>
                                                            <a data-src="{{ asset('uploads/images/temp/photography/streetlife-1.jpg') }}" class="image-popup"><img src="{{ asset('uploads/images/temp/photography/streetlife-1.jpg') }}" class="all streetlife"/></a>
                                                            <a data-src="{{ asset('uploads/images/temp/photography/streetlife-2.jpg') }}" class="image-popup"><img src="{{ asset('uploads/images/temp/photography/streetlife-2.jpg') }}" class="all streetlife film"/></a>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div id="albums-tab" class="q-albums-tab-home">

                                                    <div class="list-album-photography">
                                                        <div class="item">
                                                            <a href="#">
                                                                <div class="images-rotation" data-images='["{{ asset('uploads/images/temp/photography/journey-1.jpg') }}", "{{ asset('uploads/images/temp/photography/journey-2.jpg') }}", "{{ asset('uploads/images/temp/photography/journey-3.jpg') }}", "{{ asset('uploads/images/temp/photography/film-1.jpg') }}"]'>
                                                                  <img src="{{ asset('uploads/images/temp/photography/journey-1.jpg') }}" alt="">
                                                                </div>

                                                                <h4>Xuyen Viet 2016</h4>
                                                                <h5>#quanghuynhh #withthe10daysjourney</h5>
                                                            </a>
                                                                
                                                        </div>

                                                        <div class="item">
                                                            <a href="#">
                                                                <div class="images-rotation" data-images='["{{ asset('uploads/images/temp/photography/film-1.jpg') }}", "{{ asset('uploads/images/temp/photography/film-2.jpg') }}", "{{ asset('uploads/images/temp/photography/film-3.jpg') }}", "{{ asset('uploads/images/temp/photography/streetlife-1.jpg') }}"]'>
                                                                  <img src="{{ asset('uploads/images/temp/photography/film-1.jpg') }}" alt="">
                                                                </div>

                                                                <h4>Film Photos</h4>
                                                                <h5>All film photos made by me</h5>
                                                            </a>
                                                                
                                                        </div>

                                                        <div class="item">
                                                            <a href="#">
                                                                <div class="images-rotation" data-images='["{{ asset('uploads/images/temp/photography/landscape-1.jpg') }}", "{{ asset('uploads/images/temp/photography/landscape-2.jpg') }}", "{{ asset('uploads/images/temp/photography/landscape-3.jpg') }}"]'>
                                                                  <img src="{{ asset('uploads/images/temp/photography/landscape-1.jpg') }}" alt="">
                                                                </div>

                                                                <h4>Landscape</h4>
                                                                <h5>Somewhere in Vietnam</h5>
                                                            </a>
                                                                
                                                        </div>
                                                    </div>
                                                            

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="container">
                            <div class="order-wrap fl-wrap color-bg">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h4>Do you like this?</h4
 >                                   </div>
                                    <div class="col-md-4">
                                        <a href="{{ route('frontend.photography.index') }}" class="ord-link">See All Photos</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="parallax-title right-pos" data-scrollax="properties: { translateY: '-350px' }">F.O.T.O</div>
                    </section>
                    <!-- section end -->       


                    <!--=============== section stories  ===============-->  
                    <section id="stories" data-scrollax-parent="true" class="scroll-con-sec dec-sec">
                        <div class="container">
                            <div class="section-title">
                                <h2>Stories</h2>
                                <p>Nhật ký được viết bằng đôi mắt chân thành</p>
                                <div class="clearfix"></div>
                                <span class="bold-separator"></span>
                            </div>
                            <div class="skills-wrap">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="blog-content">
                                            <div class="blog-content classic-view">
                                                <div class="blog-posts">

                                                    
                                                    <div class="post">
                                                        <div class="box">
                                                            <div class="post-content">
                                                                <h2 class="post-title text-center">
                                                                    <a href="blog-post.html">Ligula Tristique Malesuada Venenatis Fermentum</a>
                                                                </h2>
                                                                <div class="meta text-center">
                                                                    <span class="date">12 Nov 2014</span>
                                                                    <span class="comments"><a href="#">3 Comments</a></span>
                                                                    <span class="category"><a href="#">Urban</a>, <a href="#">Conceptual</a></span></div>
                                                                <div class="fotorama-wrapper">
                                                                    <div class="fotorama"
                                                                       data-arrows="false"
                                                                       data-nav="thumbs"
                                                                         data-maxheight="100%"
                                                                       data-transition="slide"
                                                                       data-thumbwidth="90"
                                                                       data-thumbheight="60"> 

                                                                       <img src="{{ asset('uploads/images/temp/blogs/bf1.jpg') }}" data-caption="Rusty blue van in the woods" alt="" /> 
                                                                       <img src="{{ asset('uploads/images/temp/blogs/bf2.jpg') }}" data-caption="Green bike on the street" alt="" /> 
                                                                       <img src="{{ asset('uploads/images/temp/blogs/bf3.jpg') }}" data-caption="A look at the busy street" alt="" /> 
                                                                       <img src="{{ asset('uploads/images/temp/blogs/bf4.jpg') }}" data-caption="Seeing through the train" alt="" /> 
                                                                       <img src="{{ asset('uploads/images/temp/blogs/bf5.jpg') }}" data-caption="Empty train streets" alt="" /> 
                                                                       <img src="{{ asset('uploads/images/temp/blogs/bf6.jpg') }}" data-caption="Late night at the subway station" alt="" /> 
                                                                       <img src="{{ asset('uploads/images/temp/blogs/bf7.jpg') }}" data-caption="Riding bicycle with the bestie" alt="" /> 
                                                                    </div>
                                                                  <!-- /.fotorama --> 
                                                                </div>
                                                                <!-- /.fotorama-wrapper -->
                                                                <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient. Curabitur blandit tempus lacinia odio. Nullam quis risus eget urna mollis ornare vel eu leo. Vestibulum id ligula porta felis euismod semper. Nullam id dolor id nibh ultricies vehicula. Nulla vitae elit libero, a pharetra augue. Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Praesent commodo cursus magna, vel scelerisque nisl et. Donec ullamcorper nulla non metus auctor fringilla. Nullam id dolor id nibh ultricies.</p>
                                                                <p>Etiam porta sem malesuada magna mollis euismod. Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Integer posuere erat a ante venenatis dapibus posuere. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. </p>
                                                                <div class="text-center"><a href="blog-post.html" class="more">Read More</a></div>
                                                            </div>
                                                            <!-- /.post-content --> 
                                                        </div>
                                                        <!-- /.box --> 
                                                            
                                                    </div>
                                                    <!-- /.post -->



                                                    <div class="post">
                                                        <div class="box">
                                                            <div class="post-content">
                                                                <h2 class="post-title text-center"><a href="blog-post.html">Tellus Adipiscing Nibh Mattis Ligula</a></h2>
                                                                <div class="meta text-center"><span class="date">12 Nov 2014</span><span class="comments"><a href="#">7 Comments</a></span><span class="category"><a href="#">Still Life</a></span></div>
                                                                <figure class="overlay-juno">
                                                                    <a href="blog-post.html">
                                                                        <img src="{{ asset('uploads/images/temp/blogs/bc1.jpg') }}" alt="" />
                                                                    </a>
                                                                </figure>
                                                                <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient. Curabitur blandit tempus lacinia odio. Nullam quis risus eget urna mollis ornare vel eu leo. Vestibulum id ligula porta felis euismod semper. Nullam id dolor id nibh ultricies vehicula. Nulla vitae elit libero, a pharetra augue. Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Praesent commodo cursus magna, vel scelerisque nisl et. Donec ullamcorper nulla non metus auctor fringilla. Nullam id dolor id nibh ultricies.</p>
                                                                <p>Etiam porta sem malesuada magna mollis euismod. Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Integer posuere erat a ante venenatis dapibus posuere. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. </p>
                                                                <div class="text-center"><a href="blog-post.html" class="more">Read More</a></div>
                                                            </div>
                                                            <!-- /.post-content --> 
                                                        </div>
                                                        <!-- /.box --> 

                                                    </div>
                                                    <!-- /.post -->

                                                    <div class="post">
                                                        <div class="box">
                                                            <div class="post-content">
                                                                <h2 class="post-title text-center"><a href="blog-post.html">Fringilla Quam Bibendum Magna Ullamcorper</a></h2>
                                                                <div class="meta text-center"><span class="date">12 Nov 2014</span><span class="comments"><a href="#">9 Comments</a></span><span class="category"><a href="#">Still Life</a></span></div>
                                                                <figure class="media-wrapper player">
                                                                    <iframe width="560" height="232" src="https://www.youtube.com/embed/A1gwjIv99Vc?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen></iframe>
                                                                </figure>
                                                                <p>Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Sed posuere consectetur est at lobortis. Donec sed odio dui. Sed posuere consectetur est at lobortis. Vestibulum id ligula porta felis euismod semper.</p>
                                                                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Nulla vitae elit libero, a pharetra augue... </p>
                                                                <div class="text-center"><a href="blog-post.html" class="more">Read More</a></div>
                                                            </div>
                                                            <!-- /.post-content --> 
                                                        </div>
                                                        <!-- /.box --> 

                                                    </div>
                                                    <!-- /.post --> 



                                                </div>
                                                <!-- /.blog-posts --> 
                                            </div>
                                            

                                        </div>
                                        <!-- /.blog-content -->
                                                  
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="parallax-title right-pos" data-scrollax="properties: { translateY: '-350px' }">Stories</div>
                    </section>
                    <!-- section end -->



                    <!--=============== section timeline  ===============-->  
                    <section id="timeline" data-scrollax-parent="true" class="scroll-con-sec dec-sec">
                        <div class="container">
                            <div class="section-title">
                                <h2>Timeline</h2>
                                <p>Dòng thời gian: Các hoạt động, sự kiện và thông báo cá nhân</p>
                                <div class="clearfix"></div>
                                <span class="bold-separator"></span>
                            </div>
                            <div class="skills-wrap">
                                <div class="row">
                                    <div class="custom-inner-holder">
                                    <!-- 1 -->  
                                    <div class="custom-inner">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="resum-header workres">
                                                    <i class="fa fa-calendar"></i>
                                                    <h3>Thiện nguyện Thiên Phước</h3>
                                                    <span>Jan 2017</span>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <h4>Đi thăm mái ấm trẻ khuyết tật Thiên Phước</h4>
                                                <p></p>
                                                <span class="custom-inner-dec"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 1 -->
                                </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="parallax-title right-pos" data-scrollax="properties: { translateY: '-350px' }">Timeline</div>
                    </section>
                    <!-- section end -->


                    <!--  to top  -->  
                    <div class="small-sec fl-wrap">
                        <div class="to-top-wrap"><a class="to-top" href="#"> <i class="fa fa-angle-double-up"></i> Back to Top</a></div>
                    </div>
                    <!-- to top end--> 
                </div>
                <!-- content end -->
            </div>
            <!-- column-wrap end -->

            <!-- arrow nav -->
            <div class="arrowpagenav"></div>
            <!-- arrow nav end-->

@endsection



@section('css')
@parent
    <!-- <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'> -->
    <link href="{{ asset('assets/frontend/plugins/tabslet/css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/frontend/plugins/animated-masonry-gallery-with-filters/css/icomoon.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/frontend/plugins/animated-masonry-gallery-with-filters/css/animated-masonry-gallery.css') }}" rel="stylesheet" type="text/css" />

    <!-- For Stories Layout -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/plugins/juno/css/plugins.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/plugins/juno/css/q-custom.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/plugins/juno/css/color/navy.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/plugins/juno/type/icons.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,300' rel='stylesheet' type='text/css'>

@endsection


@section('js')
	@parent
    <script type="text/javascript" src="{{ asset('assets/frontend/plugins/tabslet/js/jquery.tabslet.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/frontend/plugins/images-rotation/js/jquery.images-rotation.js') }}"></script>


    <!-- <script type="text/javascript" src="{{ asset('assets/frontend/js/jquery-ui-1.10.4.min.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('assets/frontend/plugins/animated-masonry-gallery-with-filters/js/jquery.isotope.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/frontend/plugins/animated-masonry-gallery-with-filters/js/animated-masonry-gallery.js') }}"></script>



    <script type="text/javascript" src="{{ asset('assets/frontend/plugins/juno/js/q-custom-plugins.js') }}"></script> 
    <script type="text/javascript" src="{{ asset('assets/frontend/plugins/juno/js/q-custom-scripts.js') }}"></script>

@endsection