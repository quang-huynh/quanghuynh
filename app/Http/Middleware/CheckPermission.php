<?php namespace App\Http\Middleware;

// First copy this file into your middleware directoy

use Closure;

use App\Http\Models\Entities\User;
use App\Http\Backend\Http\Services\UserService;

class CheckPermission {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! config('site.check_permission_middleware')) {
            return $next($request);
        }

        
        // Get the required roles from the route
        $route_name = $this->getRouteName($request->route());

        $userService = new UserService();

        // Check if a role is required for the route, and
        // if so, ensure that the user has that role.
        if ($userService->hasPermission($request->user(), $route_name) || !$route_name)
        {
            return $next($request);
        }

        abort(401);
        // return response([
        //     'error' => [
        //         'code' => 'INSUFFICIENT_PERMISSION',
        //         'description' => 'You are not authorized (permission) to access this resource.'
        //     ]
        // ], 401);

    }

    /**
     * Get Required Permission of Route
     * @param  object $route Current Route
     * @return array
     */
    private function getRouteName($route)
    {
        $actions = $route->getAction();
        return isset($actions['as']) ? $actions['as'] : null;
    }

}