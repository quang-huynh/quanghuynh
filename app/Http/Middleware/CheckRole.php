<?php namespace App\Http\Middleware;

// First copy this file into your middleware directoy

use Closure;

use App\Http\Models\Entities\User;
use \App\Http\Backend\Http\Services\UserService;

class CheckRole {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get the required roles from the route
        $roles = $this->getRequiredRoleForRoute($request->route());

        $userService = new UserService($request->user());


        // Check if a role is required for the route, and
        // if so, ensure that the user has that role.
        if($userService->hasRole($roles) || !$roles)
        {
            return $next($request);
        }
        return response([
            'error' => [
                'code' => 'INSUFFICIENT_ROLE',
                'description' => 'You are not authorized to access this resource.'
            ]
        ], 401);

    }

    /**
     * Get Required Role of Route
     * @param  object $route Current Route
     * @return array
     */
    private function getRequiredRoleForRoute($route)
    {
        $actions = $route->getAction();
        return isset($actions['roles']) ? $actions['roles'] : null;
    }

}