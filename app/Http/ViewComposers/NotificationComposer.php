<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Http\Services\NotificationService;

use App\Http\Models\Repositories\NotificationRepository;

use Auth;

class NotificationComposer {

    protected $notificationService;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(NotificationService $notificationService)
    {
        // Dependencies automatically resolved by service container...
        $this->notificationService = $notificationService;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $readedNotifications = $this->notificationService->getReadedNotificationOfUser();
        $view->with('readed_notifications', $readedNotifications);

        $newNotifications = $this->notificationService->getUnreadNotificationOfUser();
        $view->with('new_notifications', $newNotifications);
    }

}