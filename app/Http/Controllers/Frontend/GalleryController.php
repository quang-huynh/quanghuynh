<?php 

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;


class GalleryController extends Controller {


	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}


	public function index()
	{
		return view('frontend.gallery.index');
	}

}
