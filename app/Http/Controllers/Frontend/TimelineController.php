<?php 

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;


class TimelineController extends Controller {


	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}


	public function index()
	{
		return view('frontend.timeline.index');
	}

}
