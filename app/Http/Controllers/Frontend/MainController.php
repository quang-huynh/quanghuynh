<?php 

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use Cookie;

class MainController extends Controller {


	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}


	public function hello()
	{
		Cookie::queue('isNotNewGuest', true, 1209600);
		return view('frontend.main.hello');	
	}


	public function homepage()
	{
		if (!empty(\Cookie::get('isNotNewGuest'))) {
			return view('frontend.main.home');
		} else {
			return \Redirect::route('frontend.hello');
		}


		
	}


	public function about()
	{
		return view('frontend.main.about');
	}


	public function contact()
	{
		return view('frontend.main.contact');
	}

}
