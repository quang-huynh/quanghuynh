<?php namespace App\Http\Controllers\Backend;


use Illuminate\HttpRequest;

use App\Http\Backend\Http\Services\UserService;
use App\Http\Models\Entities\User;
use App\Http\Models\Repositories\UserRepository;
use App\Http\Models\Entities\Role;
use App\Http\Models\Repositories\RoleRepository;
use App\Http\Models\Entities\Permission;
use App\Http\Models\Repositories\PermissionRepository;
use App\Http\Backend\Http\Requests\AddUserRequest;
use App\Http\Backend\Http\Requests\UpdateUserRequest;
use App\Http\Backend\Http\Requests\PermissionUserRequest;
use App\Http\Backend\Http\Requests\ChangePasswordUserRequest;

use Request;

class UserController extends BaseController {

    protected $userRepository;

    protected $roleRepository;

    protected $userService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository, UserService $userService)
    {
        //$this->middleware('guest');
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->userService = $userService;
        
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return 
     */
    public function index()
    {
        $this->buildBreadcrumb(array(
            array('name' => 'User', 'url' => \URL::route('user.index'))
        ));

        if (Request::isMethod('post')) {

            $keyword = Request::get('keyword');
            $query = $this->userService->search($keyword);
            //$query = $user->autoCompleteSearch();

            $users = $query->paginate(config('site.paginator'));


            //search

            return view('backend.user.index')->with('users', $users);
        } else {
            $users = $this->userRepository->getPaginated(20);
            return view('backend.user.index')->with('users', $users);
        }
    }

    /**
     * Detail User
     * @return 
     */
    public function detail($id)
    {
        $this->buildBreadcrumb(array(
            array('name' => 'User', 'url' => \URL::route('user.index')),
            array('name' => 'Detail', 'url' => \URL::route('user.detail', array('id' => $id)))
        ));

        $user = $this->userRepository->getById($id);
        if (!$id || !$user) {
            abort(404);
        }

        return view('backend.user.detail')->with('user',$user);
    }

    /**
     * Add User
     */
    public function add(AddUserRequest $request, RoleRepository $roleRepository) 
    {
        $this->buildBreadcrumb(array(
            array('name' => 'User', 'url' => \URL::route('user.index')),
            array('name' => 'Add', 'url' => \URL::route('user.add'))
        ));

        if ($request->isMethod('post'))
        {
            $result = $this->userRepository->add();

            if ($result) {

                $roleOfUser = $result->roles()->getResults();

                $updateRoleUser = $this->userRepository->updateRoleOfUser($result, $roleOfUser, $request);

            } else {
                abort(500);
            }

            return redirect()->route('user.index');

        } else {
            $roles = $roleRepository->getAll();

            return view('backend.user.add')->with('roles', $roles);
        }
        
    }

    /**
     * Update User
     * @return View 
     */
    public function update($id, UpdateUserRequest $request, UserRepository $userRepository, RoleRepository $roleRepository) 
    {
        $this->buildBreadcrumb(array(
            array('name' => 'User', 'url' => \URL::route('user.index')),
            array('name' => 'Update', 'url' => \URL::route('user.update', array('id' => $id)))
        ));

        $user = $this->userRepository->getById($id);

        if (!$id || !$user) {
            abort(404);
        }

        //check root user 
        if ($user->name == 'Root' && Auth::user()->name != 'Root') {
            abort(500);
        }

        if ($request->isMethod('post'))
        {
            $result = $this->userRepository->update($id);

            if ($result) {
                $user = $userRepository->getById($id);
                $roleOfUser = $user->roles()->getResults();

                $updateRoleUser = $this->userRepository->updateRoleOfUser($user, $roleOfUser, $request);

                return redirect()->route('user.index');
            } else {
                abort(500);
            }         

        } else {
            $roles = $roleRepository->getAll();
            return view('backend.user.update')->with('user',$user)->with('roles', $roles);
        }
    }

    /**
     * Delete User
     * @return View
     */
    public function delete($id) 
    {
        $user = $this->userRepository->getById($id);

        if (!$id || !$user) {
            abort(404);
        }

        //check root user 
        if ($user->name == 'Root' && Auth::user()->name != 'Root') {
            abort(500);
        }

        $result = $this->userRepository->delete($user);

        if ($result) {
            return redirect()->route('user.index');
        } else {
            abort(500);
        }

    }

    /**
     * Searching by Ajax
     * @return array
     */
    public function searchingByAjax()
    {
        $keyword = Request::get('keyword');
        $result = $this->userService->autoCompleteSearch($keyword);
        $arrResult = array();
        foreach ($result->get() as $item) {
            if (preg_match("/$keyword/",$item->fullname)) {
                $arrResult[] = $item->fullname;
            }
            if (preg_match("/$keyword/",$item->email)) {
                $arrResult[] = $item->email;
            }
            
        }

        return $arrResult;
    }


    public function resetPassword($id)
    {

        if (!$id) {
            abort(404);
        }

        if ($this->userService->resetPassword($id)) {
            return redirect()->route('user.index');
        } else {
            abort(500);
        }
    }

}
