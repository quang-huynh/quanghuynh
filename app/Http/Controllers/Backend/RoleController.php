<?php namespace App\Http\Controllers\Backend;


use Illuminate\Http\Request;

use App\Http\Backend\Http\Services\UserService;
use App\Http\Models\Entities\Role;
use App\Http\Models\Repositories\RoleRepository;
use App\Http\Backend\Http\Requests\AddRoleRequest;
use App\Http\Backend\Http\Requests\UpdateRoleRequest;


class RoleController extends BaseController {

    protected $roleRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
        
    }

    /**
     * Show the application dashboard to the role.
     *
     * @return 
     */
    public function index()
    {
        $this->buildBreadcrumb(array(
            array('name' => 'Role', 'url' => \URL::route('role.index')),
        ));

        $roles = $this->roleRepository->getPaginated(config('site.paginator'));
        return view('backend.role.index')->with('roles',$roles);
    }

    /**
     * Detail Role
     * @return 
     */
    public function detail($id)
    {
        $this->buildBreadcrumb(array(
            array('name' => 'Role', 'url' => \URL::route('role.index')),
            array('name' => 'Detail', 'url' => \URL::route('role.detail', array('id' => $id)))
        ));

        $role = $this->role->getById($id);
        if (!$id || !$role) {
            abort(404);
        }

        return view('backend.role.detail')->with('role',$role);
    }

    /**
     * Add Role
     */
    public function add(AddRoleRequest $request) 
    {
        $this->buildBreadcrumb(array(
            array('name' => 'Role', 'url' => \URL::route('role.index')),
            array('name' => 'Add', 'url' => \URL::route('role.update'))
        ));

        if ($request->isMethod('post'))
        {

            $result = $this->roleRepository->add();

            if ($result) {
                return redirect()->route('role.index');
            } else {
                abort(500);
            }

        } else {
            return view('backend.role.add');
        }
        
    }

    /**
     * Update Role
     * @return View
     */
    public function update($id, UpdateRoleRequest $request) 
    {
        $this->buildBreadcrumb(array(
            array('name' => 'Role', 'url' => \URL::route('role.index')),
            array('name' => 'Update', 'url' => \URL::route('role.update', array('id' => $id)))
        ));

        $role = $this->roleRepository->getById($id);

        if (!$id || !$role) {
            abort(404);
        }

        if ($request->isMethod('post'))
        {

            $result = $this->roleRepository->update($id);

            if ($result) {
                return redirect()->route('role.index');
            } else {
                abort(500);
            }            

        } else {
            return view('backend.role.update')->with('role',$role);
        }
    }

    /**
     * Delete Role
     * @return View
     */
    public function delete($id) 
    {
        $role = $this->roleRepository->getById($id);

        if (!$id || !$role) {
            abort(404);
        }

        $result = $this->roleRepository->delete($role);

        if ($result) {
            return redirect()->route('role.index');
        } else {
            abort(500);
        }
        
    }

}
