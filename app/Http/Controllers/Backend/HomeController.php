<?php namespace App\Http\Controllers\Backend;

use Auth;

use App\Http\Models\Repositories\UserRepository;


class HomeController extends BaseController {


	protected $userRepository;
	protected $notificationRepository;
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(UserRepository $userRepository)
	{
		$this->middleware('auth');

		$this->userRepository = $userRepository;
	}


	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('backend.dashboard');
	}

	public function logout() 
	{
		Auth::logout();
		return redirect()->route('auth.login');
	}

	public function testSendMail()
	{
		return redirect()->route('admin.index');
	}

}
