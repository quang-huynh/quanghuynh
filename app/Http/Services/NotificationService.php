<?php namespace App\Http\Services;

use App\Http\Models\Repositories\UserRepository;
use App\Http\Models\Repositories\NotificationRepository;
use App\Http\Models\Repositories\SubscribeEventRepository;

use Auth;

class NotificationService {

    protected $userRepository;
    protected $notificationRepository;
    protected $subscribeEventRepository;

    public function __construct(UserRepository $userRepository, NotificationRepository $notificationRepository, SubscribeEventRepository $subscribeEventRepository) 
    {
        $this->userRepository = $userRepository;
        $this->notificationRepository = $notificationRepository;
        $this->subscribeEventRepository = $subscribeEventRepository;
    }



    public function readed($notificationId)
    {
        $notification = $this->userRepository->hasReadedNotification($notificationId);

        //If not exist
        if (!$notification) {
            Auth::user()->notifications()->attach($notificationId, ['status' => 'readed']);
            return 'success';
        }

        return null;
    }

    public function getReadedNotificationOfUser($take = 0)
    {
        $notifications = [];
        $rolesUser = Auth::user()->roles()->getResults();

        foreach ($rolesUser as $role) {
            $subEvents = $role->subscribeEvents()->getResults();

            foreach ($subEvents as $subEvent) {
                $notificationsForSubEvent = $this->notificationRepository->getAllByAttributeOrderBy('subscribe_event_id', $subEvent->id, 'created_at')->toArray();
                foreach ($notificationsForSubEvent as $index => $notification) {
                    if ( empty( $this->userRepository->hasReadedNotification($notification['id']) ) ) {
                        unset($notificationsForSubEvent[$index]);
                    }
                }

                $notifications = array_merge($notifications,$notificationsForSubEvent);
            }
        }

        return array_slice($notifications, $take , config('site.default_number_notification'));
    }


    public function getUnreadNotificationOfUser($take = 0)
    {
        $notifications = [];
        $rolesUser = Auth::user()->roles()->getResults();

        foreach ($rolesUser as $role) {
            $subEvents = $role->subscribeEvents()->getResults();

            foreach ($subEvents as $subEvent) {
                $notificationsForSubEvent = $this->notificationRepository->getAllByAttributeOrderBy('subscribe_event_id', $subEvent->id, 'created_at')->toArray();
                foreach ($notificationsForSubEvent as $index => $notification) {
                    if ( !empty( $this->userRepository->hasReadedNotification($notification['id']) ) ) {
                        unset($notificationsForSubEvent[$index]);
                    }
                }

                $notifications = array_merge($notifications,$notificationsForSubEvent);
            }

        }

        return array_slice($notifications, $take , config('site.default_number_notification'));
    }

}