<?php namespace App\Http\Models\Services;

use Illuminate\Support\Str;

use Toolkit;

use App\Http\Models\Repositories\ProductRepository;
use App\Http\Models\Repositories\CategoryRepository;
use App\Http\Models\Entities\Category;

class ProductService 
{
	protected $productRepository;

	public function __construct(ProductRepository $repo)
	{
		$this->productRepository = $repo;
	}

	/**
	 * Update product attributes via ajax method
	 * @param  array  $input     	Ajax data
	 * @param  string $fieldName 	Attribute name
	 * @return array            	[status, content]	
	 */
	public function ajaxUpdateCategoryByAttribute(array $input, $fieldName) 
	{
		$attr = [ $fieldName => $input['value']];
		$id = $input['id'];
		$result = [];
		if ($this->productRepository->updateProductByAttribute($id, $attr)) {
			
			$result['status'] = true;
			$result['content'] = $input['value'];

			if ($fieldName == 'status') {
				$result['content'] = $this->getStatusDescription($input['value']);
			}

			if ($fieldName == 'category_id') {
				$result['content'] = $this->getCategoryName($input['value']);
			}

		} else {
			$result['status'] = false;
			$result['content'] = $input['old_data'];
		}
		return $result;
	}

	/**
	 * [save description]
	 * @param  array  $input  	Data Array
	 * @return boolean        	True/False
	 */
	public function save(array $input)
	{
		if (empty($input['slug'])) {
			$slug = Str::slug($input['name']);	
		} else {
			$slug = Str::slug($input['slug']);
		}
		$input['slug'] = $slug;

		if (isset($input['image'])) {
			$file = $input['image'];
		}

		unset($input['_token']);
		unset($input['image']);

		$product = $this->productRepository->create($input);

		if (isset($file)) {
			$product->image = Toolkit::uploadFile($product->getId(), $file);
		}
		return $product->save();
	}

	/**
	 * [update description]
	 * @param  integer $id   Product Id
	 * @param  array  $input Data array
	 * @return boolean       True/False
	 */
	public function update($id, array $input)
	{
		unset($input['_token']);	//remove _token from array
		if (isset($input['image'])) {
			$input['image'] = Toolkit::uploadFile($id,$input['image']);
		}
		if (empty($input['slug'])) {
			$slug = Str::slug($input['name']);	
		} else {
			$slug = Str::slug($input['slug']);
		}
		$input['slug'] = $slug;
		return $this->productRepository->updateProductByAttribute($id, $input);
	}

	/**
	 * Get Description of product status
	 * @param  integer $num Status value
	 * @return string      	Status description
	 */
	public function getStatusDescription($num) 
	{
		switch ($num) {
			case 1:
				return 'Published';
				break;
			
			default:
				return 'Disabled';
				break;
		}
	}

	/**
	 * Return category name
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getCategoryName($id)
	{
		$repo = new CategoryRepository(new Category);
		$category = $repo->getById($id);
		return $category->name;
	}



}