<?php namespace App\Http\Services\Backend;

use App\Http\Models\Entities\Role;
use App\Http\Models\Entities\Permission;
use App\Http\Models\Repositories\RoleRepository;

use Request;

class RoleService {

    public function __construct() 
    {
        //
    }


    /**
     * [convertCollectionToArray description]
     * @param  Collection $permissionsOfRole Collection Object of Role's Permission
     * @return array
     */
    public static function convertCollectionToArray($permissionsOfRole) 
    {
        if (count($permissionsOfRole) > 0) {
            foreach ($permissionsOfRole as $index => $permissionOfRole) {
                $arrPermissionOfRole[$index] = $permissionOfRole->id;
            }
        } else {
            $arrPermissionOfRole = array();
        }

        return $arrPermissionOfRole;
    }


    /**
     * Update Permission of selected Role
     * @param  Model $role              Role model
     * @param   $permissionsOfRole
     * @param  Custom Request $request           
     * @return 
     */
    public static function updatePermissionOfSelectedRole($role, $permissionsOfRole, $request)
    {
        $arrPermissionOfRole = self::convertCollectionToArray($permissionsOfRole);


        
        $permissionInputs = $request->input('permission');
        if (count($permissionInputs) > 0) {
            $permissionInputs = array_map(function($item) {
                return (int)$item;
            }, $permissionInputs);
        }

        //list permission must set new
        $list_must_set = array_diff($permissionInputs ? $permissionInputs : array(), $arrPermissionOfRole);


        //list permission must unset
        $list_must_unset = array_diff($arrPermissionOfRole, $permissionInputs ? $permissionInputs : array());

        if (count($list_must_unset)) {
            $role->permissions()->detach($list_must_unset);
        }
        

        $role->permissions()->attach($list_must_set);
    }



}


?>
