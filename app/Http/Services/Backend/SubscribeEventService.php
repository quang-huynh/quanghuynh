<?php namespace App\Http\Services\Backend;

use App\Http\Models\Repositories\SubscribeEventRepository;

class SubscribeEventService {

    protected $subscribeEventRepository;

    public function __construct(SubscribeEventRepository $subscribeEventRepository) 
    {
        $this->subscribeEventRepository = $subscribeEventRepository;
    }


    /**
     * [convertCollectionToArray description]
     * @param  Collection $subscribeEventsOfRole Collection Object of Role's Subscribe Event
     * @return array
     */
    public static function convertCollectionToArray($subscribeEventsOfRole) 
    {
        if (count($subscribeEventsOfRole) > 0) {
            foreach ($subscribeEventsOfRole as $index => $subscribeEventOfRole) {
                $arrSubscribeEventOfRole[$index] = $subscribeEventOfRole->id;
            }
        } else {
            $arrSubscribeEventOfRole = array();
        }

        return $arrSubscribeEventOfRole;
    }


    /**
     * Update Subscribe Event of selected Role
     * @param  Model $subscribeEvent      Subscribe Event model
     * @param   $subscribeEventsOfRole
     * @param  Custom Request $request           
     * @return 
     */
    public static function updatePermissionOfSelectedRole($subscribeEvent, $subscribeEventsOfRole, $request)
    {
        $arrSubscribeEventOfRole = self::convertCollectionToArray($subscribeEventsOfRole);


        
        $roleInputs = $request->input('roles');
        if (count($roleInputs) > 0) {
            $roleInputs = array_map(function($item) {
                return (int)$item;
            }, $roleInputs);
        }


        //list permission must set new
        $list_must_set = array_diff($roleInputs ? $roleInputs : array(), $arrSubscribeEventOfRole);


        //list permission must unset
        $list_must_unset = array_diff($arrSubscribeEventOfRole, $roleInputs ? $roleInputs : array());

        if (count($list_must_unset)) {
            $subscribeEvent->roles()->detach($list_must_unset);
        }
        

        $subscribeEvent->roles()->attach($list_must_set);
    }

}


?>
