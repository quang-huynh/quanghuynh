<?php namespace App\Http\Services\Backend;

use App\Http\Models\Entities\User;
use App\Http\Models\Repositories\UserRepository;
use App\Http\Models\Entities\Permission;
use App\Http\Models\Entities\PermissionRepository;

use Hash;

class UserService {

    protected $user;

    protected $userRepository;

    public function __construct(UserRepository $userRepository = null) 
    {
        $this->userRepository = $userRepository;
    }

    /**
     * 
     * @param  Collection $roleOfUser Collection Object of Role's Permission
     * @return array
     */
    public static function convertCollectionToArray($roleOfUser) 
    {
        if (count($roleOfUser) > 0) {
            foreach ($roleOfUser as $index => $role) {
                $arrroleOfUser[$index] = $role->id;
            }
        } else {
            $arrroleOfUser = array();
        }

        return $arrroleOfUser;
    }

    /**
     * Check Role User
     * @param $roles Roles Route Required
     * @return boolean
     */
    public function hasRole($roles)
    {
        $this->have_role = $this->getUserRole();
        
        // Check if the user is a root account
        if($this->have_role->name == 'Root') {
            return true;
        }

        if(is_array($roles)){
            foreach($roles as $need_role){
                if($this->checkIfUserHasRole($need_role)) {
                    return true;
                }
            }
        } else{
            return $this->checkIfUserHasRole($roles);
        }
        return false;
    }


    /**
     * Check Permission User
     * @param $permissions Roote name
     * @return boolean
     */
    public function hasPermission($user, $route_name)
    {
        $this->user = $user;
        $userRoles = $this->getUserRole();

        //User is Root
        if ($this->isRoot($userRoles)) {
            return true;
        }
        $this->have_permission = $this->getPermissionOfRole();
        if($route_name) {
            foreach($this->have_permission as $permission) {
                if ($permission->getRouteName() == $route_name ) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Get Role of Current User
     * @return Array/String Roles of User
     */
    private function getUserRole()
    {
        return $this->user->roles()->getResults();
    }

    /**
     * Check User has Root Role
     * @param  Collection  $userRoles
     * @return boolean
     */
    public function isRoot($userRoles = null)
    {
        if ($userRoles) {
            foreach($userRoles as $role) {
                if ($role->name == 'Root') {
                    return true;
                }
            }
        } else {
            return false;
        }
    }


    /**
     * Get Permission of Current User
     * @return Array Permissions of Role (of User)
     */
    public function getPermissionOfRole()
    {
        $arrAllPermissionOfRoles = array();

        foreach ($this->user->roles()->getResults() as $role) { //Get Collection Roles of User
            $permissions = $role->permissions()->getResults(); // Get Collection Permissions of Per Role (of User)
            foreach ($permissions as $permission) { //Get Per Permission in Collection
                $arrAllPermissionOfRoles[] = $permission; // Push in array
            }
        }

        return $arrAllPermissionOfRoles;
    }

    /**
     * Check exist User's role in role route Required
     * @param   $need_role 
     * @return boolean
     */
    private function checkIfUserHasRole($need_role)
    {
        return (strtolower($need_role)==strtolower($this->have_role->name)) ? true : false;
    }

    /**
     * Check exist User's role in role route Required
     * @param   $need_role 
     * @return boolean
     */
    private function checkIfUserHasPermission($need_permission, $permission_user)
    {
        return (strtolower($need_permission)==strtolower($permission_user)) ? true : false;
    }

    /**
     * Get Default Password
     * @return string
     */
    private function getDefaultPassword()
    {
        return config('site.default_password');
    }

    /**
     * Hash Default Password
     * @param  string $default_password
     * @return string
     */
    private function hashDefaultPassword($default_password)
    {
        return Hash::make($default_password);
    }

    /**
     * Reset old password and change to default password
     * @return boolean
     */
    public function resetPassword($id)
    {
        $user = $this->userRepository->getById($id);
        $default_password = $this->hashDefaultPassword($this->getDefaultPassword());
        $user->password = $default_password;
        return $user->save();
    }


    /**
     * Search User
     * @param  array  $params
     * @param
     * @return collection
     */
    public function search($keyword = '')
    {
        
        $arrQuery = [
                        'order_by' =>  ['created_at' => 'DESC'],
                        'query1' => "LOWER(fullname) LIKE BINARY LOWER('%{$keyword}%')",
                        'query2' => "LOWER(email) LIKE BINARY LOWER('%{$keyword}%')"
                    ];

        return $this->userRepository->queryWhereRaw($arrQuery);
    }


    public function autoCompleteSearch($keyword = '')
    {

        $arrQuery = [
                        'order_by' => ['created_at' => 'DESC'],
                        'query1' => "LOWER(fullname) LIKE BINARY LOWER('%{$keyword}%')",
                        'query2' => "LOWER(email) LIKE BINARY LOWER('%{$keyword}%')"
                    ];

        return $this->userRepository->queryWhereRaw($arrQuery);
    }


}


?>