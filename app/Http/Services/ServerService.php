<?php namespace App\Http\Services;



class ServerService 
{

    /**
     * Call to server and send data
     * @param  array $arr
     * @return 
     */
    public static function callSocketServer($arr)
    {
        $context = new \ZMQContext();
        $socket = $context->getSocket(\ZMQ::SOCKET_PUSH, 'my pusher');
        $socket->connect("tcp://127.0.0.1:5555");
        $socket->send(json_encode($arr));

    }

}
