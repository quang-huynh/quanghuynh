<?php namespace App\Http\Services;

use App\Http\Models\Repositories\UserRepository;

use Auth;

class UserService 
{

    protected $userRepository;

    public function __construct(UserRepository $userRepository) 
    {
        $this->userRepository = $userRepository;
    }

    public static function getStringRoleOfUser()
    {
        $collectionRolesUser = Auth::user()->roles()->getResults();
        $stringRoleId = '';
        foreach ($collectionRolesUser as $index => $role) {
            if ($index == 0) {
                $stringRoleId .= config('site.subscribe_name') . $role->id;
            } else {
                $stringRoleId .= ',' . config('site.subscribe_name') . $role->id;
            }
        }

        return $stringRoleId;

    }

}
