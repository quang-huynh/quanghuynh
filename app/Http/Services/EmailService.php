<?php namespace App\Http\Services;

use Mail;
use PDF;


class EmailService {

    public static function testSendMail($template_name,$array = [])
    {
        $data = [
            'info' => $array,
            'name' => $array['name'],
            'email' => $array['email'],
            'title' => 'This is an example email',
            'body'  => '',
            'data' => $array['data'],
        ];
        return Mail::send($template_name, $data, function($message) use($array) {
            $message->to($array['email'],$array['first_name'].$array['last_name'])
                    ->subject('This is an example email !');
        });
    }


    public static function sendOrderSupplier($template_name,$array = [], $filename)
    {
        $data = [
            'info' => $array,
            'title' => 'Supplier Order',
            'body'  => '',
            'data' => $array['data'],
        ];


        return Mail::send($template_name, $data, function($message) use($array, $filename) {
            $message->to($array['email'],$array['name'])
                    ->subject('Supplier Order');

            $message->attach(config('site.download_path') . $filename);

        });
    }

    public static function sendActivation($user) {
        $data = [
            'user'  => $user,
            'title' => 'Thank for your registration',
            'body'  => 'To complete your registration, please visit this URL:',
        ];

        return Mail::send('common.emails.account.registration', $data, function($message) use($user) {
            $message->to($user->email, $user->getDisplayName())
                    ->subject('Your activation code');
        });
    }

    public static function sendResetPassword($user) {
        $data = [
            'user'  => $user,
            'name'  => $user['attributes']['first_name'] . ' '. $user['attributes']['last_name'],
            'url'   => \URL::route('account.resetpassword', array($user->email, $user->reset_password_code)),
            'title' => "Renew your password",
            'body'  => "To complete renew your password, please visit this URL:",
        ];
        return Mail::send('common.emails.account.SendEmailResetPassword', $data, function($message) use($user) {
            $message->to($user->email, $user->getDisplayName())
                    ->subject('Reset Password');
        });
    }

    public static function sendContactToAdmin($msg)
    {
        $topic = Topic::find($msg->topic_id);
        $user  = User::find($msg->user_id);

        $data = [
            'msg'      => $msg,
            'topic'    => $topic = isset($topic) ? $topic : null,
            'user'     => $user  = isset($user) ? $user : null,
        ];
        return Mail::send('common.emails.contact.contact', $data, function($message) use($msg) {
            $message->to(\Config::get('site.email_admin'),'')
                    ->subject($msg->subject);
        });
    }

}