<?php


Route::any('/', [
        'as' => 'frontend.homepage', 
        'uses' => '\App\Http\Controllers\Frontend\MainController@homepage',
        
]);

Route::any('/hello', [
        'as' => 'frontend.hello', 
        'uses' => '\App\Http\Controllers\Frontend\MainController@hello',
        
]);

Route::any('/about', [
        'as' => 'frontend.aboutme', 
        'uses' => '\App\Http\Controllers\Frontend\MainController@about',
        
]);

Route::any('/contact', [
        'as' => 'frontend.contact', 
        'uses' => '\App\Http\Controllers\Frontend\MainController@contact',
        
]);

Route::any('/journey', [
        'as' => 'frontend.journey.index', 
        'uses' => '\App\Http\Controllers\Frontend\JourneyController@index',
        
]);

Route::any('/photography', [
        'as' => 'frontend.photography.index', 
        'uses' => '\App\Http\Controllers\Frontend\GalleryController@index',
        
]);

Route::any('/story', [
        'as' => 'frontend.story.index', 
        'uses' => '\App\Http\Controllers\Frontend\StoryController@index',
        
]);

// Route::any('/story/{slug}', [
Route::any('/story/detail', [
        'as' => 'frontend.story.detail', 
        'uses' => '\App\Http\Controllers\Frontend\StoryController@detail',
        
]);

Route::any('/timeline', [
        'as' => 'frontend.timeline.index', 
        'uses' => '\App\Http\Controllers\Frontend\TimelineController@index',
        
]);


// Backend

Route::any('/login', [
        'as' => 'auth.login', 
        'uses' => '\App\Http\Controllers\Backend\Auth\AuthController@login',
        
]);

Route::any('/register', [
        'as' => 'auth.register', 
        'uses' => '\App\Http\Controllers\Backend\Auth\AuthController@register',
        
]);

Route::any('/logout', [
        'as' => 'auth.logout', 
        'uses' => '\App\Http\Controllers\Backend\HomeController@logout',
        
]);


Route::controllers([
    'auth' => '\App\Http\Controllers\Backend\Auth\AuthController',
    'password' => '\App\Http\Controllers\Backend\Auth\PasswordController',
]);


Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function()
{
    Route::any('/', [
        'as' => 'admin.index', 
        'uses' => '\App\Http\Controllers\Backend\HomeController@index',
        
    ]);



});


//Route::auth();
