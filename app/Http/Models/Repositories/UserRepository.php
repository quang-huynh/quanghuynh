<?php namespace App\Http\Models\Repositories;

use App\Http\Models\Entities\User;

use App\Http\Backend\Http\Services\UserService;

use Request;

use Auth;

class UserRepository extends BaseRepository {

    protected $model;

	public function __construct(User $model)
    {
        $this->model = $model;
    }


    /**
     * Add User in Backend
     * @return
     */
    public function add()
    {
        $arrData = [
            'fullname' => Request::get('fullname'),
            'email' => Request::get('email'),
            'password' => bcrypt(Request::get('password')),
            'phone' =>  Request::get('phone'),
            'status' => Request::get('status'),

        ];
        $instance = $this->createModelInstance($arrData);

        $this->save($instance);

        return $instance;
    }

    /**
     * Update User in Backend
     * @return
     */
    public function update($userId)
    {
        $user = $this->getById($userId);

        if (!$user) {
            return false;
        }
        $user->fullname = Request::get('fullname');
        if (Request::get('email') != $user->email) {
            $user->email = Request::get('email');
        }
        $user->status = Request::get('status');
        return $this->save($user);
    }

    /**
     * Change password User
     * @return
     */
    public function changepassword($userId)
    {
        $user = $this->getById($userId);

        if (!$user) {
            return false;
        }
        $user->password = bcrypt(Request::get('password'));
        return $this->save($user);
    }


    /**
     * 
     */
    public function getPaginated($limit) 
    {
        return $this->model->orderBy('created_at', 'DESC')->paginate($limit);
    }



    

    public static function updateRoleOfUser($user, $roleOfUser, $request)
    {

        $userServices = new UserService();
        $arrRolesOfUser = $userServices->convertCollectionToArray($roleOfUser);

        
        $RoleIds = $request->input('role_id');
        if (count($RoleIds) > 0) {
            $RoleIds = array_map(function($item) {
                return (int)$item;
            }, $RoleIds);
        }

        //$array_merge = array_merge($arrRolesOfUser, $request);

        //list role must set new
        $list_must_set = array_diff($RoleIds ? $RoleIds : array(), $arrRolesOfUser);

        //list role must unset
        $list_must_unset = array_diff($arrRolesOfUser, $RoleIds ? $RoleIds : array());

        if (count($list_must_unset)) {
            $user->roles()->detach($list_must_unset);
        }
        

        return $user->roles()->attach($list_must_set);
    }

    /**
     * Create New Query
     * @return Query Builder
     */
    public function getQuery()
    {
        return $this->getModel()->newQuery();
    }

    /**
     * [queryWhereRaw description]
     * @param  array  $arrQuery Array queries
     * @return Query Builder
     */
    public function queryWhereRaw($arrQuery = [])
    {
        $user = $this->getQuery();
        $i = 0;
        foreach ($arrQuery as $key => $query) {
            if ($i == 0) {
                if ($key == 'order_by') {
                    foreach ($query as $column => $sort) {
                        $user->orderBy($column, $sort);
                    }
                    continue;
                } else {
                    $user->whereRaw($query);
                }
            } else {
                if ($key == 'order_by') {
                    foreach ($query as $column => $sort) {
                        $user->orderBy($column, $sort);
                    }
                } else {
                    $user->orWhereRaw($query);
                }
            }   
            $i++;

        }
        return $user;
        
    }


    /**
     * Check has readed notification
     * @param  integer  $notificationId 
     * @return model
     */
    public function hasReadedNotification($notificationId)
    {
        return Auth::user()->notifications()->where('notification_id', $notificationId)->where('status', 'readed')->first();
    }

}