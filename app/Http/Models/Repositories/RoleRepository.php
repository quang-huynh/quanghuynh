<?php namespace App\Http\Models\Repositories;

use App\Http\Models\Entities\Role;

use Request;

class RoleRepository extends BaseRepository {

	public function __construct(Role $model)
    {
        $this->model = $model;
    }

    public function getPaginated($limit) 
    {
        return $this->model->orderBy('created_at', 'DESC')->paginate($limit);
    }

    /**
     * Add Role in Backend
     * @return
     */
    public function add()
    {
        $arrData = [
            'name' => Request::get('name'),
            'description' => Request::get('description'),

        ];
        $instance = $this->createModelInstance($arrData);

        $this->save($instance);

        return $instance;
    }

    /**
     * Update Role in Backend
     * @return
     */
    public function update($roleId)
    {
        $role = $this->getById($roleId);

        if (!$role) {
            return false;
        }

        $role->name = Request::get('name');
        $role->description = Request::get('description');

        return $this->save($role);
    }

}