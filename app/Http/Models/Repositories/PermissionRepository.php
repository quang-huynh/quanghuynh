<?php namespace App\Http\Models\Repositories;

use App\Http\Models\Entities\Permission;

use Request;

class PermissionRepository extends BaseRepository {

	public function __construct(Permission $model)
    {
        $this->model = $model;
    }

    public function getPaginated($limit) 
    {
        return $this->model->orderBy('created_at', 'DESC')->paginate($limit);
    }


    /**
     * Add Permission in Backend
     * @return
     */
    public function add()
    {
        $arrData = [
            'title' => Request::get('title'),
            'route_name' => Request::get('route_name'),
            'description' => Request::get('description'),
        ];
        $instance = $this->createModelInstance($arrData);

        $this->save($instance);

        return $instance;
    }

    /**
     * Update Permission in Backend
     * @return
     */
    public function update($permisionId)
    {

        $permission = $this->getById($permisionId);

        if (!$permission) {
            return false;
        }

        $permission->title = Request::input('title');
        $permission->description = Request::input('description');
        $permission->route_name = Request::input('route_name');
        
        return $this->save($permission);

    }

}