<?php namespace App\Http\Common\Models\Entities;

class Role extends Base {

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'role';

	/**
	 * Define relationship: Users have many Roles, Roles have many Users.
	 * 
	 * @return App\Http\Common\Models\Entities\User Collection 
	 */
	public function users()
	{
		return $this->belongsToMany('App\Http\Common\Models\Entities\User', 'role_user', 'role_id', 'user_id');
	}

	/**
	 * Define relationship: Roles have many Permissions, Permissions have many Roles.
	 * 
	 * @return App\Http\Common\Models\Entities\Permission Collection 
	 */
	public function permissions()
	{
		return $this->belongsToMany('App\Http\Common\Models\Entities\Permission', 'permission_role', 'role_id', 'permission_id');
	}


	/**
     * Define relationship: SubscribeEvents have many Roles, Roles have many SubscribeEvents.
     * 
     * @return App\Http\Common\Models\Entities\SubscribeEvent Collection 
     */
    public function subscribeEvents()
    {
        return $this->belongsToMany('App\Http\Common\Models\Entities\SubscribeEvent', 'subscribe_event_role', 'role_id', 'subscribe_event_id');
    }


	/**
	 * Get name attribute
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Get description attribute
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

}