<?php namespace App\Http\Models\Entities;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Base implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'fullname', 
		'email', 
		'phone', 
		'password', 

		];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];


	/**
	 * Define relationship: Users have many Notifications, Notifications have many Users.
	 * 
	 * @return App\Http\Models\Entities\Notification Collection
	 */
	public function notifications()
	{
		return $this->belongsToMany('App\Http\Models\Entities\Notification', 'notification_user', 'user_id', 'notification_id');
	}

	/**
	 * Define relationship: Users have many Roles, Roles have many Users.
	 * 
	 * @return App\Http\Models\Entities\Role Collection
	 */
	public function roles()
	{
		return $this->belongsToMany('App\Http\Models\Entities\Role', 'role_user', 'user_id', 'role_id');
	}


	/**
	 * Get fullname attribute
	 * @return string
	 */
	public function getFullName()
	{
		return $this->fullname;
	}
	
	/**
	 * Get email attribute
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * Get phone attribute
	 * @return string
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * Get password attribute
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}

}