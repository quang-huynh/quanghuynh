<?php namespace App\Http\Models\Entities;

use Illuminate\Database\Eloquent\Model;

abstract class Base extends Model {

    public function getId()
    {
        return $this->id;
    }

    /**
     * Get created_at attribute
     * @return carbon
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Get updated_at attribute
     * @return carbon
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

}
