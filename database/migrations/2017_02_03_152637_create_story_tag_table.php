<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoryTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('story_tag', function($table) {
            $table->string('story_id')->unsigned();
            $table->string('tag_id')->unsigned();
            $table->primary(array('story_id', 'tag_id'), 'story_tag_primary');
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('story_tag');
    }
}
