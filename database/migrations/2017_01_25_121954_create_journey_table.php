<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJourneyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journey', function($table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->string('short_description')->nullable();
            $table->string('location')->nullable();
            $table->dateTime('time')->nullable();
            $table->text('content')->nullable();
            $table->string('thumbnail_image')->nullable();
            $table->integer('status')->nullable();
            $table->date('publish_date')->nullable();
            $table->integer('created_by')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('journey');
    }
}
