<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkSocialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_social', function($table) {
            $table->increments('id');
            $table->string('object_id')->nullable(); //ID: prefix_<id>
            $table->string('object_type')->nullable();
            $table->string('social_name')->nullable();
            $table->string('social_link')->nullable();
            $table->boolean('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('link_social');
    }
}
