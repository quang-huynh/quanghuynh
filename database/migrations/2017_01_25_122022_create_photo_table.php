<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photo', function($table) {
            $table->increments('id');
            $table->string('slug')->nullable();
            $table->string('name')->nullable();
            $table->string('size')->nullable();
            $table->string('resolution')->nullable();
            $table->integer('status')->nullable();
            $table->string('photo_by')->nullable();
            $table->integer('uploaded_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('photo');
    }
}
