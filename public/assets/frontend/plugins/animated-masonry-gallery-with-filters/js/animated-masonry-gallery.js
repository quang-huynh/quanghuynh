$(window).load(function () {

var size = 1;
var button = 1;
var button_class = "gallery-header-center-right-links-current";
var normal_size_class = "gallery-content-center-normal";
var full_size_class = "gallery-content-center-full";
var $container = $('#gallery-content-center');
    
$container.isotope({itemSelector : 'img'});


function check_button(){
	$('.gallery-header-center-right-links').removeClass(button_class);
	if(button==1){
		$("#filter-all").addClass(button_class);
		$("#gallery-header-center-left-title").html('All New Photos');
		}
	if(button==2){
		$("#filter-film").addClass(button_class);
		$("#gallery-header-center-left-title").html('Film Photos');
		}
	if(button==3){
		$("#filter-journey").addClass(button_class);
		$("#gallery-header-center-left-title").html('Journey Photos');
		}	
	if(button==4){
		$("#filter-portrait").addClass(button_class);
		$("#gallery-header-center-left-title").html('Portrait Photos');
		}	
	if(button==5){
		$("#filter-landscape").addClass(button_class);
		$("#gallery-header-center-left-title").html('Landscape Photos');
		}	
	if(button==6){
		$("#filter-streetlife").addClass(button_class);
		$("#gallery-header-center-left-title").html('Streelife Photos');
		}	
	// if(button==7){
	// 	$("#filter-creative").addClass(button_class);
	// 	$("#gallery-header-center-left-title").html('Creative Presents');
	// 	}	
}
	
function check_size(){
	$("#gallery-content-center").removeClass(normal_size_class).removeClass(full_size_class);
	if(size==0){
		$("#gallery-content-center").addClass(normal_size_class); 
		$("#gallery-header-center-left-icon").html('<span class="iconb" data-icon="&#xe23a;"></span>');
		}
	if(size==1){
		$("#gallery-content-center").addClass(full_size_class); 
		$("#gallery-header-center-left-icon").html('<span class="iconb" data-icon="&#xe23b;"></span>');
		}
	$container.isotope({itemSelector : 'img'});
}


	
$("#filter-all").click(function() { $container.isotope({ filter: '.all' }); button = 1; check_button(); });
$("#filter-film").click(function() {  $container.isotope({ filter: '.film' }); button = 2; check_button();  });
$("#filter-journey").click(function() {  $container.isotope({ filter: '.journey' }); button = 3; check_button();  });
$("#filter-portrait").click(function() {  $container.isotope({ filter: '.portrait' }); button = 4; check_button();  });
$("#filter-landscape").click(function() {  $container.isotope({ filter: '.landscape' }); button = 5; check_button();  });
$("#filter-streetlife").click(function() {  $container.isotope({ filter: '.streetlife' }); button = 6; check_button();  });
// $("#filter-creative").click(function() {  $container.isotope({ filter: '.creative' }); button = 7; check_button();  });
$("#gallery-header-center-left-icon").click(function() { if(size==0){size=1;}else if(size==1){size=0;} check_size(); });


check_button();
check_size();
});