/* ---------------------------------------------

Javascript

--------------------------------------------- */

(function($) {
    "use strict";

    // opens the menu
    $( ".toggle-menu" ).click(function() {
      $(".overlay").toggleClass("open");
    });

    // closes the menu when any link inside is clicked
    $('.overlay a').click(function() {
        $(".overlay").removeClass("open");
    });

    // opens options panel
    $('.toggle-options').click(function() {
        $(".options").toggleClass("opt-open");
    });

    // toggle light and dark themes
    $( ".slider-theme" ).click(function() {
      $("body").toggleClass("light");
    });


    // adds class of .md-opened to body
    $('.md-trigger').click(function() {
        $("body").addClass("md-opened");
    });

    //removes class of .md-opened on the body
    $('.md-close').click(function() {
        $("body").removeClass("md-opened");
    });


    /////////////////////////////////////////////////////////////End


    //Fullpage plugin
      $('#fullpage').fullpage({
          //Navigation
          menu: '#menu',
          lockAnchors: false,
          navigation: true,
          navigationPosition: 'right',
          showActiveTooltip: false,
          slidesNavigation: true,
          slidesNavPosition: 'bottom',

          //Scrolling
          css3: true,
          scrollingSpeed: 1000,
          autoScrolling: true,
          fitToSection: true,
          fitToSectionDelay: 0,
          scrollBar: false,
          easing: 'easeOutQuart',
          easingcss3: 'cubic-bezier(0.7,0,0.3,1)',
          loopBottom: false,
          loopTop: false,
          loopHorizontal: true,
          continuousVertical: true,
          normalScrollElements: '.md-modal, .photo-menu, .options',
          scrollOverflow: false,
          touchSensitivity: 15,
          normalScrollElementTouchThreshold: 5,

          //Accessibility
          keyboardScrolling: true,
          animateAnchor: true,
          recordHistory: true,

          //Design
          controlArrows: true,
          verticalCentered: true,
          resize : true,
          responsiveWidth: 0,
          responsiveHeight: 0,

          //Custom selectors
          sectionSelector: '.section',
          slideSelector: '.slide',
      });
    /////////////////////////////////////////////////////////////End


    // Up and Down Links
    $(document).on('click', '#moveUp', function(){
      $.fn.fullpage.moveSectionUp();
    });
    $(document).on('click', '#moveDown', function(){
      $.fn.fullpage.moveSectionDown();
    });
    /////////////////////////////////////////////////////////////End


})(jQuery);